/**
 * @copyright 2021 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * deputyapi.js
 * @NApiVersion 2.1
 * @NModuleScope Public
 */
define(["require", "exports", "N/https", "N/log", "N/cache", "N/error"], function (require, exports, https, log, cache, error) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.DeputyAPI = void 0;
    // noinspection JSUnusedGlobalSymbols
    class DeputyAPI {
        constructor({ clientId, clientSecret, version, refreshToken, endPoint, loggingEnabled, newRefreshToken, redirectURL }) {
            this.redirectURL = undefined;
            this.newRefreshToken = undefined;
            this.version = 'v1';
            this.endPoint = undefined;
            this.authCode = undefined;
            this.loggingEnabled = false;
            this.clientId = clientId;
            this.clientSecret = clientSecret;
            this.version = version || 'v1';
            if (!refreshToken || refreshToken.length === 0) {
                /** If we're not provided a RefreshToken, we should be attempting to get an AuthCode or we lost our RefreshToken.  Either way, we should not continue to use the current Access Token and force the enduser to Reauthenticate right away. */
                this.clearAccessToken();
            }
            else {
                this.refreshToken = refreshToken;
            }
            this.endPoint = endPoint;
            this.loggingEnabled = loggingEnabled || false;
            this.newRefreshToken = newRefreshToken;
            this.redirectURL = redirectURL;
        }
        static get AccessTokenCache() {
            return cache.getCache({ name: 'Deputy_AccessToken', scope: cache.Scope.PROTECTED });
        }
        set EnableLogging(toggle) {
            this.loggingEnabled = toggle;
        }
        /** Provides the URL to request Authentication */
        get AuthenticationURL() {
            return `https://once.deputy.com/my/oauth/login?client_id=${this.clientId}&redirect_uri=${this.RedirectURI}&response_type=code&scope=longlife_refresh_token`;
        }
        /** Setting the AuthCode will force a refresh of the AccessToken and RefreshToken */
        set AuthCode(authCode) {
            this.authCode = authCode;
            this.AccessToken;
        }
        get RedirectURI() {
            if (this.redirectURL) {
                return this.redirectURL();
            }
            else {
                throw error.create({ name: 'RedirectURI', message: 'You must provide a Redirect URL' });
            }
        }
        /** @internal */
        get AccessToken() {
            let accessToken = DeputyAPI.AccessTokenCache.get({ key: this.clientId });
            /** Force a re-fetch of an AccessToken if we've been given an AuthCode */
            if (accessToken && accessToken.length > 5 && !this.authCode) {
                this.log('audit', 'AccessToken', 'Using cached Access Token');
            }
            else {
                accessToken = undefined;
                const accessTokenRequestOptions = {
                    method: https.Method.POST,
                    url: undefined,
                    body: {
                        client_id: this.clientId,
                        client_secret: this.clientSecret,
                        redirect_uri: this.RedirectURI,
                        scope: 'longlife_refresh_token',
                    },
                };
                if (this.authCode) {
                    accessTokenRequestOptions.url = `https://once.deputy.com/my/oauth/access_token`;
                    accessTokenRequestOptions.body.grant_type = 'authorization_code';
                    accessTokenRequestOptions.body.code = this.authCode;
                }
                else if (this.refreshToken) {
                    accessTokenRequestOptions.url = `https://${this.endPoint}/oauth/access_token`;
                    accessTokenRequestOptions.body.grant_type = 'refresh_token';
                    accessTokenRequestOptions.body.refresh_token = this.refreshToken;
                }
                else {
                    const messageObject = {
                        name: 'No RefreshToken or AuthCode',
                        stack: undefined,
                        cause: {
                            message: 'Attempted to obtain a new Access Token but no RefreshToken was found or no AuthCode was provided.  Please reauthorize connection with Deputy',
                            name: `Reauthorization Required for ${this.endPoint}`,
                        },
                        id: undefined,
                        message: `No RefreshToken or AuthCode provided for endPoint: ${this.endPoint}`,
                    };
                    throw error.create(messageObject);
                }
                this.log('audit', 'AccessToken - accessTokenRequestOptions', accessTokenRequestOptions);
                const clientResponse = https.request(accessTokenRequestOptions);
                this.log('audit', 'AccessToken - clientResponse', clientResponse);
                switch (+clientResponse?.code) {
                    case 200: {
                        const accessTokenResponse = JSON.parse(clientResponse.body);
                        accessToken = accessTokenResponse.access_token;
                        if (this.newRefreshToken) {
                            this.log('audit', 'AccessToken - newRefreshToken', 'Notifying callback of new RefreshToken');
                            this.newRefreshToken(accessTokenResponse.endpoint.replace(/^https?:\/\//, ''), accessTokenResponse.refresh_token);
                        }
                        this.log('audit', 'AccessToken - caching', 'Caching AccessToken');
                        DeputyAPI.AccessTokenCache.put({
                            key: this.clientId,
                            value: accessToken,
                            ttl: +accessTokenResponse.expires_in - 100, // Lets expiry early, in case it took us time to get to his piece of code.
                        });
                        this.log('audit', 'AccessToken - caching', 'Access Token retrieval complete');
                        break;
                    }
                    default: {
                        throw error.create({ message: JSON.stringify(clientResponse), name: `Unsupported Response Code: ${clientResponse.code}` });
                    }
                }
                if (!accessToken) {
                    this.clearAccessToken();
                }
            }
            return accessToken;
        }
        clearAccessToken() {
            DeputyAPI.AccessTokenCache.remove({ key: this.clientId });
        }
        getPayRules(options) {
            options.resource = 'resource/PayRules';
            this.makeGETRequest(options);
        }
        getLeaveRules(options) {
            options.resource = 'resource/LeaveRules';
            this.makeGETRequest(options);
        }
        getCountries(options) {
            options.resource = 'resource/Country';
            this.makeGETRequest(options);
        }
        getStates(options) {
            options.resource = 'resource/State';
            this.makeGETRequest(options);
        }
        getEmployee(options) {
            options.resource = `supervise/employee/${options.objectId}`;
            this.makeGETRequest(options);
        }
        upsertEmployee(options) {
            options.resource = +options.objectId > 0 ? `supervise/employee/${+options.objectId}` : `supervise/employee`;
            options.payload = options.object;
            this.makePOSTRequest(options);
        }
        upsertEmployees(options) {
            options.resource = `supervise/employee/create`;
            options.payload = {
                arrEmployee: options.objects,
            };
            this.makePOSTRequest(options);
        }
        getCompany(options) {
            options.resource = `resource/Company/${options.objectId}`;
            this.makeGETRequest(options);
        }
        getCompanies(options) {
            options.resource = `resource/Company`;
            this.makeGETRequest(options);
        }
        getOperationalUnits(options) {
            options.resource = `resource/OperationalUnit/QUERY`;
            options.payload = {
                search: {
                    mappedLocation: {
                        field: 'Company',
                        type: 'in',
                        data: options.companyIds,
                    },
                },
            };
            this.makePOSTRequest(options);
        }
        setTimesheetExportStatus(options) {
            options.resource = `resource/Timesheet/${options.timeSheetId}`;
            options.payload = {
                Exported: options.status,
            };
            this.makePOSTRequest(options);
        }
        getEmployees(options) {
            options.resource = `resource/Employee/QUERY`;
            options.payload = {
                search: {
                    filterOutArchived: {
                        field: 'Active',
                        type: 'eq',
                        data: options.isActive,
                    },
                },
            };
            this.makePOSTRequest(options);
        }
        getTimeSheetPayReturns(options) {
            options.resource = `resource/TimesheetPayReturn/QUERY`;
            const timesheetPayReturnQuery = {
                search: {
                    s1: {
                        field: 'Date',
                        type: 'ge',
                        data: options.startDate,
                        join: 'TimesheetObject',
                    },
                    s2: {
                        field: 'Date',
                        type: 'le',
                        data: options.endDate,
                        join: 'TimesheetObject',
                    },
                },
                join: ['PayRuleObject', 'TimesheetObject'],
            };
            timesheetPayReturnQuery.search['operationalUnit'] =
                options.operationalUnitIds && options.operationalUnitIds.length > 0
                    ? {
                        field: 'OperationalUnit',
                        type: 'in',
                        data: options.operationalUnitIds,
                        join: 'TimesheetObject',
                    }
                    : {
                        field: 'OperationalUnit',
                        type: 'ns',
                        data: '',
                        join: 'TimesheetObject',
                    };
            if (options.excludeExported) {
                timesheetPayReturnQuery.search['s3'] = {
                    field: 'Exported',
                    type: 'ns',
                    data: '',
                    join: 'TimesheetObject',
                };
            }
            options.payload = timesheetPayReturnQuery;
            this.makePOSTRequest(options);
        }
        log(level, title, details) {
            if (this.loggingEnabled) {
                log[level]({
                    title: `DeputyAPI: ${title}`,
                    details: details,
                });
            }
        }
        /** @internal */
        makeGETRequest(options) {
            options.method = https.Method.GET;
            this.makeRequest(options);
        }
        makePOSTRequest(options) {
            options.method = https.Method.POST;
            this.makeRequest(options);
        }
        /** @internal */
        makeRequest(options) {
            let clientResponse;
            const requestOptions = {
                method: options.method,
                url: `https://${this.endPoint}/api/${this.version}/${options.resource}`,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${this.AccessToken}`,
                },
            };
            if (options.payload) {
                // ensure we're always sorting by Id
                options.payload['sort'] = { Id: 'desc' };
                if (options.start && options.start > 0) {
                    options.payload['start'] = options.start;
                }
                requestOptions.body = JSON.stringify(options.payload);
            }
            try {
                this.log('audit', `makeRequest - requestOptions`, requestOptions);
                clientResponse = https.request(requestOptions);
                this.log('audit', `makeRequest - clientResponse`, clientResponse);
                switch (clientResponse.code) {
                    case 200: {
                        let resultObjects;
                        try {
                            resultObjects = JSON.parse(clientResponse.body);
                            if (Array.isArray(resultObjects)) {
                                options.postResults = options.postResults ? [...options.postResults, ...resultObjects] : resultObjects;
                                // If we get exactly 500 results, it's likely there are more to get
                                if (resultObjects.length === 500) {
                                    options.start = options.postResults.length;
                                    this.makeRequest(options);
                                }
                                else {
                                    options.OK(options.postResults);
                                }
                            }
                            else {
                                options.OK(resultObjects);
                            }
                        }
                        catch {
                            options.OK?.(clientResponse);
                        }
                        break;
                    }
                    case 201: {
                        options.Created?.(clientResponse);
                        break;
                    }
                    case 202: {
                        options.Accepted?.(clientResponse);
                        break;
                    }
                    case 400: {
                        options.BadRequest ? options.BadRequest(clientResponse) : options.Failed?.(clientResponse);
                        break;
                    }
                    case 401: {
                        options.Unauthorized ? options.Unauthorized(clientResponse) : options.Failed?.(clientResponse);
                        break;
                    }
                    case 403: {
                        options.Forbidden ? options.Forbidden(clientResponse) : options.Failed?.(clientResponse);
                        break;
                    }
                    case 404: {
                        options.NotFound ? options.NotFound(clientResponse) : options.Failed?.(clientResponse);
                        break;
                    }
                    case 500: {
                        options.ServerError ? options.ServerError(clientResponse) : options.Failed?.(clientResponse);
                        break;
                    }
                    default: {
                        options.Failed?.(clientResponse);
                        break;
                    }
                }
            }
            catch (error_) {
                options.retryCount = options.retryCount || 0;
                if (options.retryCount <= 5) {
                    options.retryCount = options.retryCount + 1;
                    this.makeRequest(options);
                }
                else {
                    this.log('error', 'makeRequest', error_);
                    options.Failed?.(clientResponse);
                }
            }
        }
    }
    exports.DeputyAPI = DeputyAPI;
    // noinspection JSUnusedLocalSymbols
    DeputyAPI.convertSuiteletURL = (original) => {
        const queryParameters = original.split('&');
        const scriptId = queryParameters[0].split('=')[1];
        const deploymentId = queryParameters[1].split('=')[1];
        const accountId = queryParameters[2].split('=')[1];
        const h = queryParameters[3].split('=')[1];
        return `https://deputy-oauth2-poc.herokuapp.com/webhook/${accountId}/${scriptId}/${deploymentId}/${h}`;
    };
});
