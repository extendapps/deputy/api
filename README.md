# Deputy API NetSuite module

A distributable module for `Deputy` connectivity using NetSuite API's

See https://developer.deputy.com/deputy-docs/docs for more details

## Installation Instructions

`npm install --save-dev @extendapps/deputyapi`

Once installed, you'll need to make this module **relative** to you code in order to confirm to NetSuite's **relative** imports

### Dev-Ops Suggestions

Add the following commands to you "scripts" section in your package.json
> "deputyapi-setup": "cd Source; ln -s ../node_modules/\\@extendapps/deputyapi/"

> "deputyapi-deploy": "cp Source/deputyapi/deputyapi.js FileCabinet/PATH_TO_ROOT/",

### Update .gitignore

Add the following to you .gitignore file

> /Source/deputyapi/

### Create a symbolic link (Manual)

* Navigate to the root folder of you TypeScript source
* `ln -s ../node_modules/\@extendapps/deputyapi/`

This will create a **reference** to the deputyapi folder and allow you to use the appropriate import statement

`import {DeputyAPI} from './deputyapi';`

## Usage

### More Details

[Techincal Details](docs/README.md)
