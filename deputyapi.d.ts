/**
 * @copyright 2021 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * deputyapi.js
 * @NApiVersion 2.1
 * @NModuleScope Public
 */
import { EmployeeOptions, GetObjectOptions, GetObjectsOptions, OperationalUnitOptions, TimesheetExportStatusOptions, TimesheetPayReturnOptions, UpsertObjectOptions, UpsertObjectsOptions } from './Interfaces/Base';
import { Company } from './Objects/Company';
import { Country } from './Objects/Country';
import { Employee, UpsertEmployee } from './Objects/Employee';
import { PayRule } from './Objects/PayRule';
import { State } from './Objects/State';
import { TimesheetPayReturn } from './Objects/TimesheetPayReturn';
import { OperationalUnit } from './Objects/OperationalUnit';
import { TimesheetObject } from './Objects/Timesheet';
import { LeaveRule } from './Objects/LeaveRule';
export declare class DeputyAPI {
    private readonly redirectURL;
    private readonly newRefreshToken;
    private readonly clientId;
    private readonly clientSecret;
    private readonly version;
    private readonly refreshToken;
    private readonly endPoint;
    private authCode;
    private loggingEnabled;
    constructor({ clientId, clientSecret, version, refreshToken, endPoint, loggingEnabled, newRefreshToken, redirectURL }: ConstructorOptions);
    private static get AccessTokenCache();
    set EnableLogging(toggle: boolean);
    /** Provides the URL to request Authentication */
    get AuthenticationURL(): string;
    /** Setting the AuthCode will force a refresh of the AccessToken and RefreshToken */
    set AuthCode(authCode: string);
    private get RedirectURI();
    /** @internal */
    private get AccessToken();
    private static readonly convertSuiteletURL;
    clearAccessToken(): void;
    getPayRules(options: GetObjectsOptions<PayRule>): void;
    getLeaveRules(options: GetObjectsOptions<LeaveRule>): void;
    getCountries(options: GetObjectsOptions<Country>): void;
    getStates(options: GetObjectsOptions<State>): void;
    getEmployee(options: GetObjectOptions<Employee>): void;
    upsertEmployee(options: UpsertObjectOptions<UpsertEmployee, Employee>): void;
    upsertEmployees(options: UpsertObjectsOptions<UpsertEmployee, Employee>): void;
    getCompany(options: GetObjectOptions<Company>): void;
    getCompanies(options: GetObjectsOptions<Company>): void;
    getOperationalUnits(options: OperationalUnitOptions<OperationalUnit>): void;
    setTimesheetExportStatus(options: TimesheetExportStatusOptions<TimesheetObject>): void;
    getEmployees(options: EmployeeOptions<Employee>): void;
    getTimeSheetPayReturns(options: TimesheetPayReturnOptions<TimesheetPayReturn>): void;
    private log;
    /** @internal */
    private makeGETRequest;
    private makePOSTRequest;
    /** @internal */
    private makeRequest;
}
/** @internal */
export type NewRefreshTokenCallback = (endPoint: string, refreshToken: string) => void;
/** @internal */
export type CustomURL = () => string;
/** @internal */
export interface ConstructorOptions {
    clientId: string;
    clientSecret: string;
    version?: 'v1';
    refreshToken?: string;
    endPoint?: string;
    loggingEnabled?: boolean;
    newRefreshToken?: NewRefreshTokenCallback;
    redirectURL?: CustomURL;
}
