/**
 * @copyright 2021 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * deputyapi.js
 * @NApiVersion 2.1
 * @NModuleScope Public
 */

import * as https from 'N/https';
import * as log from 'N/log';
import * as cache from 'N/cache';
import * as error from 'N/error';
import {EmployeeOptions, GetObjectOptions, GetObjectsOptions, OperationalUnitOptions, TimesheetExportStatusOptions, TimesheetPayReturnOptions, UpsertObjectOptions, UpsertObjectsOptions} from './Interfaces/Base';
import {Company} from './Objects/Company';
import {Country} from './Objects/Country';
import {Employee, UpsertEmployee} from './Objects/Employee';
import {PayRule} from './Objects/PayRule';
import {State} from './Objects/State';
import {TimesheetPayReturn} from './Objects/TimesheetPayReturn';
import {OperationalUnit} from './Objects/OperationalUnit';
import {TimesheetObject} from './Objects/Timesheet';
import {LeaveRule} from './Objects/LeaveRule';

// noinspection JSUnusedGlobalSymbols
export class DeputyAPI {
    private readonly redirectURL: CustomURL = undefined;
    private readonly newRefreshToken: NewRefreshTokenCallback = undefined;
    private readonly clientId: string;
    private readonly clientSecret: string;
    private readonly version = 'v1' as const;
    private readonly refreshToken: string;
    private readonly endPoint: string = undefined;
    private authCode: string = undefined;
    private loggingEnabled = false;

    constructor({clientId, clientSecret, version, refreshToken, endPoint, loggingEnabled, newRefreshToken, redirectURL}: ConstructorOptions) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.version = version || 'v1';

        if (!refreshToken || refreshToken.length === 0) {
            /** If we're not provided a RefreshToken, we should be attempting to get an AuthCode or we lost our RefreshToken.  Either way, we should not continue to use the current Access Token and force the enduser to Reauthenticate right away. */
            this.clearAccessToken();
        } else {
            this.refreshToken = refreshToken;
        }

        this.endPoint = endPoint;
        this.loggingEnabled = loggingEnabled || false;
        this.newRefreshToken = newRefreshToken;
        this.redirectURL = redirectURL;
    }

    private static get AccessTokenCache(): cache.Cache {
        return cache.getCache({name: 'Deputy_AccessToken', scope: cache.Scope.PROTECTED});
    }

    public set EnableLogging(toggle: boolean) {
        this.loggingEnabled = toggle;
    }

    /** Provides the URL to request Authentication */
    get AuthenticationURL(): string {
        return `https://once.deputy.com/my/oauth/login?client_id=${this.clientId}&redirect_uri=${this.RedirectURI}&response_type=code&scope=longlife_refresh_token`;
    }

    /** Setting the AuthCode will force a refresh of the AccessToken and RefreshToken */
    set AuthCode(authCode: string) {
        this.authCode = authCode;
        this.AccessToken;
    }

    private get RedirectURI(): string {
        if (this.redirectURL) {
            return this.redirectURL();
        } else {
            throw error.create({name: 'RedirectURI', message: 'You must provide a Redirect URL'});
        }
    }

    /** @internal */
    private get AccessToken(): string {
        let accessToken: string = DeputyAPI.AccessTokenCache.get({key: this.clientId});

        /** Force a re-fetch of an AccessToken if we've been given an AuthCode */
        if (accessToken && accessToken.length > 5 && !this.authCode) {
            this.log('audit', 'AccessToken', 'Using cached Access Token');
        } else {
            accessToken = undefined;
            const accessTokenRequestOptions: https.RequestOptions = {
                method: https.Method.POST,
                url: undefined,
                body: {
                    client_id: this.clientId,
                    client_secret: this.clientSecret,
                    redirect_uri: this.RedirectURI,
                    scope: 'longlife_refresh_token',
                },
            };

            if (this.authCode) {
                accessTokenRequestOptions.url = `https://once.deputy.com/my/oauth/access_token`;
                accessTokenRequestOptions.body.grant_type = 'authorization_code';
                accessTokenRequestOptions.body.code = this.authCode;
            } else if (this.refreshToken) {
                accessTokenRequestOptions.url = `https://${this.endPoint}/oauth/access_token`;
                accessTokenRequestOptions.body.grant_type = 'refresh_token';
                accessTokenRequestOptions.body.refresh_token = this.refreshToken;
            } else {
                const messageObject: error.SuiteScriptError = {
                    name: 'No RefreshToken or AuthCode',
                    stack: undefined,
                    cause: {
                        message: 'Attempted to obtain a new Access Token but no RefreshToken was found or no AuthCode was provided.  Please reauthorize connection with Deputy',
                        name: `Reauthorization Required for ${this.endPoint}`,
                    },
                    id: undefined,
                    message: `No RefreshToken or AuthCode provided for endPoint: ${this.endPoint}`,
                };
                throw error.create(messageObject);
            }

            this.log('audit', 'AccessToken - accessTokenRequestOptions', accessTokenRequestOptions);
            const clientResponse: https.ClientResponse = https.request(accessTokenRequestOptions);
            this.log('audit', 'AccessToken - clientResponse', clientResponse);

            switch (+clientResponse?.code) {
                case 200: {
                    const accessTokenResponse: AccessTokenResponse = JSON.parse(clientResponse.body);
                    accessToken = accessTokenResponse.access_token;

                    if (this.newRefreshToken) {
                        this.log('audit', 'AccessToken - newRefreshToken', 'Notifying callback of new RefreshToken');
                        this.newRefreshToken(accessTokenResponse.endpoint.replace(/^https?:\/\//, ''), accessTokenResponse.refresh_token);
                    }

                    this.log('audit', 'AccessToken - caching', 'Caching AccessToken');
                    DeputyAPI.AccessTokenCache.put({
                        key: this.clientId,
                        value: accessToken,
                        ttl: +accessTokenResponse.expires_in - 100, // Lets expiry early, in case it took us time to get to his piece of code.
                    });
                    this.log('audit', 'AccessToken - caching', 'Access Token retrieval complete');
                    break;
                }

                default: {
                    throw error.create({message: JSON.stringify(clientResponse), name: `Unsupported Response Code: ${clientResponse.code}`});
                }
            }

            if (!accessToken) {
                this.clearAccessToken();
            }
        }

        return accessToken;
    }

    // noinspection JSUnusedLocalSymbols
    private static readonly convertSuiteletURL = (original: string): string => {
        const queryParameters: string[] = original.split('&');
        const scriptId: string = queryParameters[0].split('=')[1];
        const deploymentId: string = queryParameters[1].split('=')[1];
        const accountId: string = queryParameters[2].split('=')[1];
        const h: string = queryParameters[3].split('=')[1];

        return `https://deputy-oauth2-poc.herokuapp.com/webhook/${accountId}/${scriptId}/${deploymentId}/${h}`;
    };

    public clearAccessToken() {
        DeputyAPI.AccessTokenCache.remove({key: this.clientId});
    }

    public getPayRules(options: GetObjectsOptions<PayRule>) {
        (options as unknown as MakeGETRequestOptions<PayRule>).resource = 'resource/PayRules';
        this.makeGETRequest(options as unknown as MakeGETRequestOptions<PayRule>);
    }

    public getLeaveRules(options: GetObjectsOptions<LeaveRule>) {
        (options as unknown as MakeGETRequestOptions<LeaveRule>).resource = 'resource/LeaveRules';
        this.makeGETRequest(options as unknown as MakeGETRequestOptions<LeaveRule>);
    }

    public getCountries(options: GetObjectsOptions<Country>) {
        (options as unknown as MakeGETRequestOptions<Country>).resource = 'resource/Country';
        this.makeGETRequest(options as unknown as MakeGETRequestOptions<Country>);
    }

    public getStates(options: GetObjectsOptions<State>) {
        (options as unknown as MakeGETRequestOptions<State>).resource = 'resource/State';
        this.makeGETRequest(options as unknown as MakeGETRequestOptions<State>);
    }

    public getEmployee(options: GetObjectOptions<Employee>) {
        (options as unknown as MakeGETRequestOptions<Employee>).resource = `supervise/employee/${options.objectId}`;
        this.makeGETRequest(options as unknown as MakeGETRequestOptions<Employee>);
    }

    public upsertEmployee(options: UpsertObjectOptions<UpsertEmployee, Employee>) {
        (options as unknown as MakePOSTRequestOptions<Employee>).resource = +options.objectId > 0 ? `supervise/employee/${+options.objectId}` : `supervise/employee`;
        (options as unknown as MakePOSTRequestOptions<Employee>).payload = options.object;
        this.makePOSTRequest(options as unknown as MakePOSTRequestOptions<Employee>);
    }

    public upsertEmployees(options: UpsertObjectsOptions<UpsertEmployee, Employee>) {
        (options as unknown as MakePOSTRequestOptions<Employee>).resource = `supervise/employee/create`;
        (options as unknown as MakePOSTRequestOptions<Employee>).payload = {
            arrEmployee: options.objects,
        };
        this.makePOSTRequest(options as unknown as MakePOSTRequestOptions<Employee>);
    }

    public getCompany(options: GetObjectOptions<Company>) {
        (options as unknown as MakeGETRequestOptions<Company>).resource = `resource/Company/${options.objectId}`;
        this.makeGETRequest(options as unknown as MakeGETRequestOptions<Company>);
    }

    public getCompanies(options: GetObjectsOptions<Company>) {
        (options as unknown as MakeGETRequestOptions<Company>).resource = `resource/Company`;
        this.makeGETRequest(options as unknown as MakeGETRequestOptions<Company>);
    }

    public getOperationalUnits(options: OperationalUnitOptions<OperationalUnit>) {
        (options as unknown as MakePOSTRequestOptions<OperationalUnit>).resource = `resource/OperationalUnit/QUERY`;
        (options as unknown as MakePOSTRequestOptions<OperationalUnit>).payload = {
            search: {
                mappedLocation: {
                    field: 'Company',
                    type: 'in',
                    data: options.companyIds,
                },
            },
        };
        this.makePOSTRequest(options as unknown as MakePOSTRequestOptions<OperationalUnit>);
    }

    public setTimesheetExportStatus(options: TimesheetExportStatusOptions<TimesheetObject>) {
        (options as unknown as MakePOSTRequestOptions<TimesheetObject>).resource = `resource/Timesheet/${options.timeSheetId}`;
        (options as unknown as MakePOSTRequestOptions<TimesheetObject>).payload = {
            Exported: options.status,
        };
        this.makePOSTRequest(options as unknown as MakePOSTRequestOptions<TimesheetObject>);
    }

    public getEmployees(options: EmployeeOptions<Employee>) {
        (options as unknown as MakePOSTRequestOptions<Employee>).resource = `resource/Employee/QUERY`;

        (options as unknown as MakePOSTRequestOptions<Employee>).payload = {
            search: {
                filterOutArchived: {
                    field: 'Active',
                    type: 'eq',
                    data: options.isActive,
                },
            },
        };

        this.makePOSTRequest(options as unknown as MakePOSTRequestOptions<Employee>);
    }

    public getTimeSheetPayReturns(options: TimesheetPayReturnOptions<TimesheetPayReturn>) {
        (options as unknown as MakePOSTRequestOptions<TimesheetPayReturn>).resource = `resource/TimesheetPayReturn/QUERY`;

        const timesheetPayReturnQuery = {
            search: {
                s1: {
                    field: 'Date',
                    type: 'ge',
                    data: options.startDate,
                    join: 'TimesheetObject',
                },
                s2: {
                    field: 'Date',
                    type: 'le',
                    data: options.endDate,
                    join: 'TimesheetObject',
                },
            },
            join: ['PayRuleObject', 'TimesheetObject'],
        };

        timesheetPayReturnQuery.search['operationalUnit'] =
            options.operationalUnitIds && options.operationalUnitIds.length > 0
                ? {
                      field: 'OperationalUnit',
                      type: 'in',
                      data: options.operationalUnitIds,
                      join: 'TimesheetObject',
                  }
                : {
                      field: 'OperationalUnit',
                      type: 'ns',
                      data: '',
                      join: 'TimesheetObject',
                  };

        if (options.excludeExported) {
            timesheetPayReturnQuery.search['s3'] = {
                field: 'Exported',
                type: 'ns',
                data: '',
                join: 'TimesheetObject',
            };
        }

        (options as unknown as MakePOSTRequestOptions<TimesheetPayReturn>).payload = timesheetPayReturnQuery;

        this.makePOSTRequest(options as unknown as MakePOSTRequestOptions<TimesheetPayReturn>);
    }

    private log(level: 'audit' | 'error' | 'emergency' | 'debug', title: string, details: unknown): void {
        if (this.loggingEnabled) {
            log[level]({
                title: `DeputyAPI: ${title}`,
                details: details,
            });
        }
    }

    /** @internal */
    private makeGETRequest<T>(options: MakeGETRequestOptions<T>) {
        (options as unknown as MakeRequestOptions<T>).method = https.Method.GET;
        this.makeRequest(options as unknown as MakeRequestOptions<T>);
    }

    private makePOSTRequest<T>(options: MakePOSTRequestOptions<T>) {
        (options as unknown as MakeRequestOptions<T>).method = https.Method.POST;
        this.makeRequest(options as unknown as MakeRequestOptions<T>);
    }

    /** @internal */
    private makeRequest<T>(options: MakeRequestOptions<T>) {
        let clientResponse: https.ClientResponse;
        const requestOptions: https.RequestOptions = {
            method: options.method,
            url: `https://${this.endPoint}/api/${this.version}/${options.resource}`,
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${this.AccessToken}`,
            },
        };

        if (options.payload) {
            // ensure we're always sorting by Id
            options.payload['sort'] = {Id: 'desc'};
            if (options.start && options.start > 0) {
                options.payload['start'] = options.start;
            }
            requestOptions.body = JSON.stringify(options.payload);
        }

        try {
            this.log('audit', `makeRequest - requestOptions`, requestOptions);
            clientResponse = https.request(requestOptions);
            this.log('audit', `makeRequest - clientResponse`, clientResponse);

            switch (clientResponse.code) {
                case 200: {
                    let resultObjects: T | T[];
                    try {
                        resultObjects = JSON.parse(clientResponse.body);
                        if (Array.isArray(resultObjects)) {
                            options.postResults = options.postResults ? [...options.postResults, ...resultObjects] : resultObjects;

                            // If we get exactly 500 results, it's likely there are more to get
                            if (resultObjects.length === 500) {
                                options.start = options.postResults.length;
                                this.makeRequest(options);
                            } else {
                                options.OK(options.postResults);
                            }
                        } else {
                            options.OK(resultObjects);
                        }
                    } catch {
                        options.OK?.(<T>clientResponse);
                    }
                    break;
                }

                case 201: {
                    options.Created?.(clientResponse);
                    break;
                }

                case 202: {
                    options.Accepted?.(clientResponse);
                    break;
                }

                case 400: {
                    options.BadRequest ? options.BadRequest(clientResponse) : options.Failed?.(clientResponse);
                    break;
                }

                case 401: {
                    options.Unauthorized ? options.Unauthorized(clientResponse) : options.Failed?.(clientResponse);
                    break;
                }

                case 403: {
                    options.Forbidden ? options.Forbidden(clientResponse) : options.Failed?.(clientResponse);
                    break;
                }

                case 404: {
                    options.NotFound ? options.NotFound(clientResponse) : options.Failed?.(clientResponse);
                    break;
                }

                case 500: {
                    options.ServerError ? options.ServerError(clientResponse) : options.Failed?.(clientResponse);
                    break;
                }

                default: {
                    options.Failed?.(clientResponse);
                    break;
                }
            }
        } catch (error_) {
            options.retryCount = options.retryCount || 0;
            if (options.retryCount <= 5) {
                options.retryCount = options.retryCount + 1;
                this.makeRequest(options);
            } else {
                this.log('error', 'makeRequest', error_);
                options.Failed?.(clientResponse);
            }
        }
    }
}

/** @internal */
interface BaseRequestOptions {
    resource: string;

    Failed(clientResponse: https.ClientResponse): void;
}

/** @internal */
interface MakeGETRequestOptions<T> extends BaseRequestOptions {
    OK(object: T): void;
}

/** @internal */
interface MakePOSTRequestOptions<T> extends BaseRequestOptions {
    payload: unknown;

    OK(objects: T[]): void;
}

/** @internal */
interface MakeRequestOptions<T> extends BaseRequestOptions {
    method: https.Method;
    payload?: unknown;
    retryCount?: number;
    postResults?: T[];
    start?: number;

    OK?(object: T | T[]): void;

    Created?(clientResponse: https.ClientResponse): void;

    Accepted?(clientResponse: https.ClientResponse): void;

    BadRequest?(clientResponse: https.ClientResponse): void;

    Unauthorized?(clientResponse: https.ClientResponse): void;

    Forbidden?(clientResponse: https.ClientResponse): void;

    NotFound?(clientResponse: https.ClientResponse): void;

    ServerError?(clientResponse: https.ClientResponse): void;
}

/** @internal */
export type NewRefreshTokenCallback = (endPoint: string, refreshToken: string) => void;

/** @internal */
export type CustomURL = () => string;

/** @internal */
export interface ConstructorOptions {
    clientId: string;
    clientSecret: string;
    version?: 'v1';
    refreshToken?: string;
    endPoint?: string;
    loggingEnabled?: boolean;
    newRefreshToken?: NewRefreshTokenCallback;
    redirectURL?: CustomURL;
}

/** @internal */
interface AccessTokenResponse {
    access_token: string;
    expires_in: number;
    scope: string;
    endpoint: string;
    refresh_token: string;
}
