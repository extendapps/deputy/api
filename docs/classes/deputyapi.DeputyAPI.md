[Deputy API NetSuite module](../README.md) / [deputyapi](../modules/deputyapi.md) / DeputyAPI

# Class: DeputyAPI

[deputyapi](../modules/deputyapi.md).DeputyAPI

## Table of contents

### Constructors

- [constructor](deputyapi.DeputyAPI.md#constructor)

### Accessors

- [AuthCode](deputyapi.DeputyAPI.md#authcode)
- [AuthenticationURL](deputyapi.DeputyAPI.md#authenticationurl)
- [EnableLogging](deputyapi.DeputyAPI.md#enablelogging)

### Methods

- [clearAccessToken](deputyapi.DeputyAPI.md#clearaccesstoken)
- [getCompanies](deputyapi.DeputyAPI.md#getcompanies)
- [getCompany](deputyapi.DeputyAPI.md#getcompany)
- [getCountries](deputyapi.DeputyAPI.md#getcountries)
- [getEmployee](deputyapi.DeputyAPI.md#getemployee)
- [getEmployees](deputyapi.DeputyAPI.md#getemployees)
- [getLeaveRules](deputyapi.DeputyAPI.md#getleaverules)
- [getOperationalUnits](deputyapi.DeputyAPI.md#getoperationalunits)
- [getPayRules](deputyapi.DeputyAPI.md#getpayrules)
- [getStates](deputyapi.DeputyAPI.md#getstates)
- [getTimeSheetPayReturns](deputyapi.DeputyAPI.md#gettimesheetpayreturns)
- [setTimesheetExportStatus](deputyapi.DeputyAPI.md#settimesheetexportstatus)
- [upsertEmployee](deputyapi.DeputyAPI.md#upsertemployee)
- [upsertEmployees](deputyapi.DeputyAPI.md#upsertemployees)

## Constructors

### constructor

• **new DeputyAPI**(`«destructured»`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `«destructured»` | [`ConstructorOptions`](../interfaces/deputyapi.ConstructorOptions.md) |

## Accessors

### AuthCode

• `set` **AuthCode**(`authCode`): `void`

Setting the AuthCode will force a refresh of the AccessToken and RefreshToken

#### Parameters

| Name | Type |
| :------ | :------ |
| `authCode` | `string` |

#### Returns

`void`

___

### AuthenticationURL

• `get` **AuthenticationURL**(): `string`

Provides the URL to request Authentication

#### Returns

`string`

___

### EnableLogging

• `set` **EnableLogging**(`toggle`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `toggle` | `boolean` |

#### Returns

`void`

## Methods

### clearAccessToken

▸ **clearAccessToken**(): `void`

#### Returns

`void`

___

### getCompanies

▸ **getCompanies**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`GetObjectsOptions`](../interfaces/Interfaces_Base.GetObjectsOptions.md)<[`Company`](../interfaces/Objects_Company.Company.md)\> |

#### Returns

`void`

___

### getCompany

▸ **getCompany**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`GetObjectOptions`](../interfaces/Interfaces_Base.GetObjectOptions.md)<[`Company`](../interfaces/Objects_Company.Company.md)\> |

#### Returns

`void`

___

### getCountries

▸ **getCountries**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`GetObjectsOptions`](../interfaces/Interfaces_Base.GetObjectsOptions.md)<[`Country`](../interfaces/Objects_Country.Country.md)\> |

#### Returns

`void`

___

### getEmployee

▸ **getEmployee**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`GetObjectOptions`](../interfaces/Interfaces_Base.GetObjectOptions.md)<[`Employee`](../interfaces/Objects_Employee.Employee.md)\> |

#### Returns

`void`

___

### getEmployees

▸ **getEmployees**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`EmployeeOptions`](../interfaces/Interfaces_Base.EmployeeOptions.md)<[`Employee`](../interfaces/Objects_Employee.Employee.md)\> |

#### Returns

`void`

___

### getLeaveRules

▸ **getLeaveRules**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`GetObjectsOptions`](../interfaces/Interfaces_Base.GetObjectsOptions.md)<[`LeaveRule`](../interfaces/Objects_LeaveRule.LeaveRule.md)\> |

#### Returns

`void`

___

### getOperationalUnits

▸ **getOperationalUnits**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`OperationalUnitOptions`](../interfaces/Interfaces_Base.OperationalUnitOptions.md)<[`OperationalUnit`](../interfaces/Objects_OperationalUnit.OperationalUnit.md)\> |

#### Returns

`void`

___

### getPayRules

▸ **getPayRules**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`GetObjectsOptions`](../interfaces/Interfaces_Base.GetObjectsOptions.md)<[`PayRule`](../interfaces/Objects_PayRule.PayRule.md)\> |

#### Returns

`void`

___

### getStates

▸ **getStates**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`GetObjectsOptions`](../interfaces/Interfaces_Base.GetObjectsOptions.md)<[`State`](../interfaces/Objects_State.State.md)\> |

#### Returns

`void`

___

### getTimeSheetPayReturns

▸ **getTimeSheetPayReturns**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`TimesheetPayReturnOptions`](../interfaces/Interfaces_Base.TimesheetPayReturnOptions.md)<[`TimesheetPayReturn`](../interfaces/Objects_TimesheetPayReturn.TimesheetPayReturn.md)\> |

#### Returns

`void`

___

### setTimesheetExportStatus

▸ **setTimesheetExportStatus**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`TimesheetExportStatusOptions`](../interfaces/Interfaces_Base.TimesheetExportStatusOptions.md)<[`TimesheetObject`](../interfaces/Objects_Timesheet.TimesheetObject.md)\> |

#### Returns

`void`

___

### upsertEmployee

▸ **upsertEmployee**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`UpsertObjectOptions`](../interfaces/Interfaces_Base.UpsertObjectOptions.md)<[`UpsertEmployee`](../interfaces/Objects_Employee.UpsertEmployee.md), [`Employee`](../interfaces/Objects_Employee.Employee.md)\> |

#### Returns

`void`

___

### upsertEmployees

▸ **upsertEmployees**(`options`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `options` | [`UpsertObjectsOptions`](../interfaces/Interfaces_Base.UpsertObjectsOptions.md)<[`UpsertEmployee`](../interfaces/Objects_Employee.UpsertEmployee.md), [`Employee`](../interfaces/Objects_Employee.Employee.md)\> |

#### Returns

`void`
