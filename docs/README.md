Deputy API NetSuite module

# Deputy API NetSuite module

## Table of contents

### Modules

- [Interfaces/Base](modules/Interfaces_Base.md)
- [Objects/BaseObject](modules/Objects_BaseObject.md)
- [Objects/Company](modules/Objects_Company.md)
- [Objects/Country](modules/Objects_Country.md)
- [Objects/Employee](modules/Objects_Employee.md)
- [Objects/LeaveRule](modules/Objects_LeaveRule.md)
- [Objects/OperationalUnit](modules/Objects_OperationalUnit.md)
- [Objects/PayRule](modules/Objects_PayRule.md)
- [Objects/State](modules/Objects_State.md)
- [Objects/Timesheet](modules/Objects_Timesheet.md)
- [Objects/TimesheetPayReturn](modules/Objects_TimesheetPayReturn.md)
- [deputyapi](modules/deputyapi.md)
