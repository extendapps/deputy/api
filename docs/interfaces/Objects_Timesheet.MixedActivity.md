[Deputy API NetSuite module](../README.md) / [Objects/Timesheet](../modules/Objects_Timesheet.md) / MixedActivity

# Interface: MixedActivity

[Objects/Timesheet](../modules/Objects_Timesheet.md).MixedActivity

## Table of contents

### Properties

- [blnCanEndEarly](Objects_Timesheet.MixedActivity.md#blncanendearly)
- [blnCanStartEarly](Objects_Timesheet.MixedActivity.md#blncanstartearly)
- [blnIsMandatory](Objects_Timesheet.MixedActivity.md#blnismandatory)
- [intState](Objects_Timesheet.MixedActivity.md#intstate)
- [strBreakType](Objects_Timesheet.MixedActivity.md#strbreaktype)

## Properties

### blnCanEndEarly

• **blnCanEndEarly**: `number`

___

### blnCanStartEarly

• **blnCanStartEarly**: `number`

___

### blnIsMandatory

• **blnIsMandatory**: `number`

___

### intState

• **intState**: `number`

___

### strBreakType

• **strBreakType**: `string`
