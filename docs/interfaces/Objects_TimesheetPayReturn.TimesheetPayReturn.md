[Deputy API NetSuite module](../README.md) / [Objects/TimesheetPayReturn](../modules/Objects_TimesheetPayReturn.md) / TimesheetPayReturn

# Interface: TimesheetPayReturn

[Objects/TimesheetPayReturn](../modules/Objects_TimesheetPayReturn.md).TimesheetPayReturn

BaseObject is a common structure that all Deputy Resources share

## Hierarchy

- [`BaseObject`](Objects_BaseObject.BaseObject.md)

  ↳ **`TimesheetPayReturn`**

## Table of contents

### Properties

- [Cost](Objects_TimesheetPayReturn.TimesheetPayReturn.md#cost)
- [Created](Objects_TimesheetPayReturn.TimesheetPayReturn.md#created)
- [Creator](Objects_TimesheetPayReturn.TimesheetPayReturn.md#creator)
- [Id](Objects_TimesheetPayReturn.TimesheetPayReturn.md#id)
- [Modified](Objects_TimesheetPayReturn.TimesheetPayReturn.md#modified)
- [Overridden](Objects_TimesheetPayReturn.TimesheetPayReturn.md#overridden)
- [OverrideComment](Objects_TimesheetPayReturn.TimesheetPayReturn.md#overridecomment)
- [PayRule](Objects_TimesheetPayReturn.TimesheetPayReturn.md#payrule)
- [PayRuleObject](Objects_TimesheetPayReturn.TimesheetPayReturn.md#payruleobject)
- [Timesheet](Objects_TimesheetPayReturn.TimesheetPayReturn.md#timesheet)
- [TimesheetObject](Objects_TimesheetPayReturn.TimesheetPayReturn.md#timesheetobject)
- [Value](Objects_TimesheetPayReturn.TimesheetPayReturn.md#value)
- [\_DPMetaData](Objects_TimesheetPayReturn.TimesheetPayReturn.md#_dpmetadata)

## Properties

### Cost

• **Cost**: `number`

___

### Created

• **Created**: `string` \| `Date`

The date this resource was created

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Created](Objects_BaseObject.BaseObject.md#created)

___

### Creator

• **Creator**: `number`

The deputy Id for the User that created this resource

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Creator](Objects_BaseObject.BaseObject.md#creator)

___

### Id

• **Id**: `number`

The deputy resource id

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Id](Objects_BaseObject.BaseObject.md#id)

___

### Modified

• **Modified**: `string` \| `Date`

The date this resource was last modified.

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Modified](Objects_BaseObject.BaseObject.md#modified)

___

### Overridden

• **Overridden**: `boolean`

___

### OverrideComment

• **OverrideComment**: `string`

___

### PayRule

• **PayRule**: `number`

___

### PayRuleObject

• **PayRuleObject**: [`PayRule`](Objects_PayRule.PayRule.md)

___

### Timesheet

• **Timesheet**: `number`

___

### TimesheetObject

• **TimesheetObject**: [`TimesheetObject`](Objects_Timesheet.TimesheetObject.md)

___

### Value

• **Value**: `number`

___

### \_DPMetaData

• **\_DPMetaData**: [`DPMetaData`](Objects_BaseObject.DPMetaData.md)
