[Deputy API NetSuite module](../README.md) / [Interfaces/Base](../modules/Interfaces_Base.md) / TimesheetExportStatusOptions

# Interface: TimesheetExportStatusOptions<T\>

[Interfaces/Base](../modules/Interfaces_Base.md).TimesheetExportStatusOptions

## Type parameters

| Name |
| :------ |
| `T` |

## Hierarchy

- `BaseOptions`

  ↳ **`TimesheetExportStatusOptions`**

## Table of contents

### Properties

- [status](Interfaces_Base.TimesheetExportStatusOptions.md#status)
- [timeSheetId](Interfaces_Base.TimesheetExportStatusOptions.md#timesheetid)

### Methods

- [Failed](Interfaces_Base.TimesheetExportStatusOptions.md#failed)
- [OK](Interfaces_Base.TimesheetExportStatusOptions.md#ok)

## Properties

### status

• **status**: `boolean`

___

### timeSheetId

• **timeSheetId**: `number`

## Methods

### Failed

▸ **Failed**(`clientResponse`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `clientResponse` | `ClientResponse` |

#### Returns

`void`

#### Inherited from

BaseOptions.Failed

___

### OK

▸ **OK**(`object`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `object` | `T` |

#### Returns

`void`
