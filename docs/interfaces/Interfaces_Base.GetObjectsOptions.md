[Deputy API NetSuite module](../README.md) / [Interfaces/Base](../modules/Interfaces_Base.md) / GetObjectsOptions

# Interface: GetObjectsOptions<T\>

[Interfaces/Base](../modules/Interfaces_Base.md).GetObjectsOptions

## Type parameters

| Name |
| :------ |
| `T` |

## Hierarchy

- `BaseOptions`

  ↳ **`GetObjectsOptions`**

## Table of contents

### Methods

- [Failed](Interfaces_Base.GetObjectsOptions.md#failed)
- [OK](Interfaces_Base.GetObjectsOptions.md#ok)

## Methods

### Failed

▸ **Failed**(`clientResponse`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `clientResponse` | `ClientResponse` |

#### Returns

`void`

#### Inherited from

BaseOptions.Failed

___

### OK

▸ **OK**(`objects`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `objects` | `T`[] |

#### Returns

`void`
