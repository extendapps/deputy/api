[Deputy API NetSuite module](../README.md) / [Interfaces/Base](../modules/Interfaces_Base.md) / EmployeeOptions

# Interface: EmployeeOptions<T\>

[Interfaces/Base](../modules/Interfaces_Base.md).EmployeeOptions

## Type parameters

| Name |
| :------ |
| `T` |

## Hierarchy

- `BaseOptions`

  ↳ **`EmployeeOptions`**

## Table of contents

### Properties

- [isActive](Interfaces_Base.EmployeeOptions.md#isactive)

### Methods

- [Failed](Interfaces_Base.EmployeeOptions.md#failed)
- [OK](Interfaces_Base.EmployeeOptions.md#ok)

## Properties

### isActive

• **isActive**: `boolean`

## Methods

### Failed

▸ **Failed**(`clientResponse`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `clientResponse` | `ClientResponse` |

#### Returns

`void`

#### Inherited from

BaseOptions.Failed

___

### OK

▸ **OK**(`objects`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `objects` | `T`[] |

#### Returns

`void`
