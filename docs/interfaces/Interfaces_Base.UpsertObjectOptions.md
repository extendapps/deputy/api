[Deputy API NetSuite module](../README.md) / [Interfaces/Base](../modules/Interfaces_Base.md) / UpsertObjectOptions

# Interface: UpsertObjectOptions<T, Y\>

[Interfaces/Base](../modules/Interfaces_Base.md).UpsertObjectOptions

## Type parameters

| Name |
| :------ |
| `T` |
| `Y` |

## Hierarchy

- `BaseOptions`

  ↳ **`UpsertObjectOptions`**

## Table of contents

### Properties

- [object](Interfaces_Base.UpsertObjectOptions.md#object)
- [objectId](Interfaces_Base.UpsertObjectOptions.md#objectid)

### Methods

- [Failed](Interfaces_Base.UpsertObjectOptions.md#failed)
- [OK](Interfaces_Base.UpsertObjectOptions.md#ok)

## Properties

### object

• **object**: `T`

___

### objectId

• `Optional` **objectId**: `number`

## Methods

### Failed

▸ **Failed**(`clientResponse`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `clientResponse` | `ClientResponse` |

#### Returns

`void`

#### Inherited from

BaseOptions.Failed

___

### OK

▸ **OK**(`object`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `object` | `Y` |

#### Returns

`void`
