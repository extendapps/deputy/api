[Deputy API NetSuite module](../README.md) / [Interfaces/Base](../modules/Interfaces_Base.md) / UpsertObjectsOptions

# Interface: UpsertObjectsOptions<T, Y\>

[Interfaces/Base](../modules/Interfaces_Base.md).UpsertObjectsOptions

## Type parameters

| Name |
| :------ |
| `T` |
| `Y` |

## Hierarchy

- `BaseOptions`

  ↳ **`UpsertObjectsOptions`**

## Table of contents

### Properties

- [objects](Interfaces_Base.UpsertObjectsOptions.md#objects)

### Methods

- [Failed](Interfaces_Base.UpsertObjectsOptions.md#failed)
- [OK](Interfaces_Base.UpsertObjectsOptions.md#ok)

## Properties

### objects

• **objects**: `T`[]

## Methods

### Failed

▸ **Failed**(`clientResponse`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `clientResponse` | `ClientResponse` |

#### Returns

`void`

#### Inherited from

BaseOptions.Failed

___

### OK

▸ **OK**(`objects`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `objects` | `Y`[] |

#### Returns

`void`
