[Deputy API NetSuite module](../README.md) / [Interfaces/Base](../modules/Interfaces_Base.md) / GetObjectOptions

# Interface: GetObjectOptions<T\>

[Interfaces/Base](../modules/Interfaces_Base.md).GetObjectOptions

## Type parameters

| Name |
| :------ |
| `T` |

## Hierarchy

- `BaseOptions`

  ↳ **`GetObjectOptions`**

## Table of contents

### Properties

- [objectId](Interfaces_Base.GetObjectOptions.md#objectid)

### Methods

- [Failed](Interfaces_Base.GetObjectOptions.md#failed)
- [OK](Interfaces_Base.GetObjectOptions.md#ok)

## Properties

### objectId

• **objectId**: `number`

## Methods

### Failed

▸ **Failed**(`clientResponse`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `clientResponse` | `ClientResponse` |

#### Returns

`void`

#### Inherited from

BaseOptions.Failed

___

### OK

▸ **OK**(`object`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `object` | `T` |

#### Returns

`void`
