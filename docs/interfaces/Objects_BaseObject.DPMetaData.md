[Deputy API NetSuite module](../README.md) / [Objects/BaseObject](../modules/Objects_BaseObject.md) / DPMetaData

# Interface: DPMetaData

[Objects/BaseObject](../modules/Objects_BaseObject.md).DPMetaData

## Table of contents

### Properties

- [CreatorInfo](Objects_BaseObject.DPMetaData.md#creatorinfo)
- [CustomFieldData](Objects_BaseObject.DPMetaData.md#customfielddata)
- [EmployeeInfo](Objects_BaseObject.DPMetaData.md#employeeinfo)
- [OperationalUnitInfo](Objects_BaseObject.DPMetaData.md#operationalunitinfo)
- [System](Objects_BaseObject.DPMetaData.md#system)

## Properties

### CreatorInfo

• **CreatorInfo**: [`CreatorInfo`](Objects_BaseObject.CreatorInfo.md)

___

### CustomFieldData

• `Optional` **CustomFieldData**: `any`

___

### EmployeeInfo

• `Optional` **EmployeeInfo**: [`EmployeeInfo`](Objects_BaseObject.EmployeeInfo.md)

___

### OperationalUnitInfo

• `Optional` **OperationalUnitInfo**: [`OperationalUnitInfo`](Objects_BaseObject.OperationalUnitInfo.md)

___

### System

• **System**: `string`
