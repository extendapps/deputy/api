[Deputy API NetSuite module](../README.md) / [Objects/PayRule](../modules/Objects_PayRule.md) / PayRule

# Interface: PayRule

[Objects/PayRule](../modules/Objects_PayRule.md).PayRule

BaseObject is a common structure that all Deputy Resources share

## Hierarchy

- [`BaseObject`](Objects_BaseObject.BaseObject.md)

  ↳ **`PayRule`**

## Table of contents

### Properties

- [AdvancedCalculation](Objects_PayRule.PayRule.md#advancedcalculation)
- [AnnualSalary](Objects_PayRule.PayRule.md#annualsalary)
- [Comment](Objects_PayRule.PayRule.md#comment)
- [Created](Objects_PayRule.PayRule.md#created)
- [Creator](Objects_PayRule.PayRule.md#creator)
- [DexmlScript](Objects_PayRule.PayRule.md#dexmlscript)
- [DexmlScriptParam](Objects_PayRule.PayRule.md#dexmlscriptparam)
- [HourlyRate](Objects_PayRule.PayRule.md#hourlyrate)
- [Id](Objects_PayRule.PayRule.md#id)
- [IsExported](Objects_PayRule.PayRule.md#isexported)
- [IsMultiplier](Objects_PayRule.PayRule.md#ismultiplier)
- [MaximumShiftLength](Objects_PayRule.PayRule.md#maximumshiftlength)
- [MaximumType](Objects_PayRule.PayRule.md#maximumtype)
- [MaximumValue](Objects_PayRule.PayRule.md#maximumvalue)
- [MinimumShiftLength](Objects_PayRule.PayRule.md#minimumshiftlength)
- [MinimumType](Objects_PayRule.PayRule.md#minimumtype)
- [MinimumValue](Objects_PayRule.PayRule.md#minimumvalue)
- [Modified](Objects_PayRule.PayRule.md#modified)
- [MultiplierBaseRate](Objects_PayRule.PayRule.md#multiplierbaserate)
- [MultiplierValue](Objects_PayRule.PayRule.md#multipliervalue)
- [PayPortionRule](Objects_PayRule.PayRule.md#payportionrule)
- [PayTitle](Objects_PayRule.PayRule.md#paytitle)
- [PayrollCategory](Objects_PayRule.PayRule.md#payrollcategory)
- [PeriodType](Objects_PayRule.PayRule.md#periodtype)
- [RecommendWith](Objects_PayRule.PayRule.md#recommendwith)
- [RemunerationBy](Objects_PayRule.PayRule.md#remunerationby)
- [RemunerationType](Objects_PayRule.PayRule.md#remunerationtype)
- [Schedule](Objects_PayRule.PayRule.md#schedule)
- [UnitValue](Objects_PayRule.PayRule.md#unitvalue)
- [\_DPMetaData](Objects_PayRule.PayRule.md#_dpmetadata)

## Properties

### AdvancedCalculation

• `Optional` **AdvancedCalculation**: `number`

___

### AnnualSalary

• `Optional` **AnnualSalary**: `number`

___

### Comment

• `Optional` **Comment**: `string`

___

### Created

• **Created**: `string` \| `Date`

The date this resource was created

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Created](Objects_BaseObject.BaseObject.md#created)

___

### Creator

• **Creator**: `number`

The deputy Id for the User that created this resource

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Creator](Objects_BaseObject.BaseObject.md#creator)

___

### DexmlScript

• `Optional` **DexmlScript**: `string`

___

### DexmlScriptParam

• `Optional` **DexmlScriptParam**: `string`

___

### HourlyRate

• `Optional` **HourlyRate**: `number`

___

### Id

• **Id**: `number`

The deputy resource id

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Id](Objects_BaseObject.BaseObject.md#id)

___

### IsExported

• **IsExported**: `boolean`

___

### IsMultiplier

• **IsMultiplier**: `boolean`

___

### MaximumShiftLength

• `Optional` **MaximumShiftLength**: `number`

___

### MaximumType

• `Optional` **MaximumType**: `number`

___

### MaximumValue

• `Optional` **MaximumValue**: `number`

___

### MinimumShiftLength

• `Optional` **MinimumShiftLength**: `number`

___

### MinimumType

• `Optional` **MinimumType**: `number`

___

### MinimumValue

• `Optional` **MinimumValue**: `number`

___

### Modified

• **Modified**: `string` \| `Date`

The date this resource was last modified.

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Modified](Objects_BaseObject.BaseObject.md#modified)

___

### MultiplierBaseRate

• `Optional` **MultiplierBaseRate**: `number`

___

### MultiplierValue

• `Optional` **MultiplierValue**: `number`

___

### PayPortionRule

• `Optional` **PayPortionRule**: `number`

___

### PayTitle

• **PayTitle**: `string`

___

### PayrollCategory

• `Optional` **PayrollCategory**: `string`

___

### PeriodType

• `Optional` **PeriodType**: `string`

___

### RecommendWith

• `Optional` **RecommendWith**: `number`

___

### RemunerationBy

• **RemunerationBy**: ``2`` \| ``1`` \| ``3``

___

### RemunerationType

• **RemunerationType**: ``2`` \| ``1`` \| ``3``

___

### Schedule

• `Optional` **Schedule**: `number`

___

### UnitValue

• `Optional` **UnitValue**: `number`

___

### \_DPMetaData

• **\_DPMetaData**: [`DPMetaData`](Objects_BaseObject.DPMetaData.md)
