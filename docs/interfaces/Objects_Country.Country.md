[Deputy API NetSuite module](../README.md) / [Objects/Country](../modules/Objects_Country.md) / Country

# Interface: Country

[Objects/Country](../modules/Objects_Country.md).Country

BaseObject is a common structure that all Deputy Resources share

## Hierarchy

- [`BaseObject`](Objects_BaseObject.BaseObject.md)

  ↳ **`Country`**

## Table of contents

### Properties

- [Active](Objects_Country.Country.md#active)
- [Code](Objects_Country.Country.md#code)
- [CodeA3](Objects_Country.Country.md#codea3)
- [Country](Objects_Country.Country.md#country)
- [Created](Objects_Country.Country.md#created)
- [Creator](Objects_Country.Country.md#creator)
- [Id](Objects_Country.Country.md#id)
- [Modified](Objects_Country.Country.md#modified)
- [PhoneDisplayPreg](Objects_Country.Country.md#phonedisplaypreg)
- [Region](Objects_Country.Country.md#region)
- [SortOrder](Objects_Country.Country.md#sortorder)
- [ZipValidatePreg](Objects_Country.Country.md#zipvalidatepreg)

## Properties

### Active

• **Active**: `boolean`

___

### Code

• **Code**: `string`

___

### CodeA3

• **CodeA3**: `string`

___

### Country

• **Country**: `string`

___

### Created

• **Created**: `string` \| `Date`

The date this resource was created

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Created](Objects_BaseObject.BaseObject.md#created)

___

### Creator

• **Creator**: `number`

The deputy Id for the User that created this resource

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Creator](Objects_BaseObject.BaseObject.md#creator)

___

### Id

• **Id**: `number`

The deputy resource id

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Id](Objects_BaseObject.BaseObject.md#id)

___

### Modified

• **Modified**: `string` \| `Date`

The date this resource was last modified.

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Modified](Objects_BaseObject.BaseObject.md#modified)

___

### PhoneDisplayPreg

• **PhoneDisplayPreg**: `string`

___

### Region

• **Region**: `string`

___

### SortOrder

• `Optional` **SortOrder**: `number`

___

### ZipValidatePreg

• **ZipValidatePreg**: `string`
