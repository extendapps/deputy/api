[Deputy API NetSuite module](../README.md) / [Objects/State](../modules/Objects_State.md) / State

# Interface: State

[Objects/State](../modules/Objects_State.md).State

BaseObject is a common structure that all Deputy Resources share

## Hierarchy

- [`BaseObject`](Objects_BaseObject.BaseObject.md)

  ↳ **`State`**

## Table of contents

### Properties

- [Active](Objects_State.State.md#active)
- [Code](Objects_State.State.md#code)
- [Country](Objects_State.State.md#country)
- [Created](Objects_State.State.md#created)
- [Creator](Objects_State.State.md#creator)
- [Id](Objects_State.State.md#id)
- [Modified](Objects_State.State.md#modified)
- [SortOrder](Objects_State.State.md#sortorder)
- [State](Objects_State.State.md#state)

## Properties

### Active

• **Active**: `boolean`

___

### Code

• **Code**: `string`

___

### Country

• **Country**: `number`

___

### Created

• **Created**: `string` \| `Date`

The date this resource was created

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Created](Objects_BaseObject.BaseObject.md#created)

___

### Creator

• **Creator**: `number`

The deputy Id for the User that created this resource

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Creator](Objects_BaseObject.BaseObject.md#creator)

___

### Id

• **Id**: `number`

The deputy resource id

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Id](Objects_BaseObject.BaseObject.md#id)

___

### Modified

• **Modified**: `string` \| `Date`

The date this resource was last modified.

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Modified](Objects_BaseObject.BaseObject.md#modified)

___

### SortOrder

• `Optional` **SortOrder**: `number`

___

### State

• **State**: `string`
