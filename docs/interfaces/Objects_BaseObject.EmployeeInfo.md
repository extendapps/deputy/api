[Deputy API NetSuite module](../README.md) / [Objects/BaseObject](../modules/Objects_BaseObject.md) / EmployeeInfo

# Interface: EmployeeInfo

[Objects/BaseObject](../modules/Objects_BaseObject.md).EmployeeInfo

## Table of contents

### Properties

- [CustomPronouns](Objects_BaseObject.EmployeeInfo.md#custompronouns)
- [DisplayName](Objects_BaseObject.EmployeeInfo.md#displayname)
- [Employee](Objects_BaseObject.EmployeeInfo.md#employee)
- [EmployeeProfile](Objects_BaseObject.EmployeeInfo.md#employeeprofile)
- [Id](Objects_BaseObject.EmployeeInfo.md#id)
- [Photo](Objects_BaseObject.EmployeeInfo.md#photo)
- [Pronouns](Objects_BaseObject.EmployeeInfo.md#pronouns)

## Properties

### CustomPronouns

• **CustomPronouns**: `string`

___

### DisplayName

• **DisplayName**: `string`

___

### Employee

• **Employee**: `number`

___

### EmployeeProfile

• **EmployeeProfile**: `number`

___

### Id

• **Id**: `number`

___

### Photo

• **Photo**: `string`

___

### Pronouns

• **Pronouns**: `number`
