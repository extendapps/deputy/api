[Deputy API NetSuite module](../README.md) / [Objects/LeaveRule](../modules/Objects_LeaveRule.md) / LeaveRule

# Interface: LeaveRule

[Objects/LeaveRule](../modules/Objects_LeaveRule.md).LeaveRule

BaseObject is a common structure that all Deputy Resources share

## Hierarchy

- [`BaseObject`](Objects_BaseObject.BaseObject.md)

  ↳ **`LeaveRule`**

## Table of contents

### Properties

- [AnnualRollOver](Objects_LeaveRule.LeaveRule.md#annualrollover)
- [Calc](Objects_LeaveRule.LeaveRule.md#calc)
- [CalcType](Objects_LeaveRule.LeaveRule.md#calctype)
- [Created](Objects_LeaveRule.LeaveRule.md#created)
- [Creator](Objects_LeaveRule.LeaveRule.md#creator)
- [Description](Objects_LeaveRule.LeaveRule.md#description)
- [EntitlementAfterMonth](Objects_LeaveRule.LeaveRule.md#entitlementaftermonth)
- [ExportType](Objects_LeaveRule.LeaveRule.md#exporttype)
- [F00](Objects_LeaveRule.LeaveRule.md#f00)
- [F01](Objects_LeaveRule.LeaveRule.md#f01)
- [F02](Objects_LeaveRule.LeaveRule.md#f02)
- [F03](Objects_LeaveRule.LeaveRule.md#f03)
- [F04](Objects_LeaveRule.LeaveRule.md#f04)
- [F05](Objects_LeaveRule.LeaveRule.md#f05)
- [F06](Objects_LeaveRule.LeaveRule.md#f06)
- [F07](Objects_LeaveRule.LeaveRule.md#f07)
- [F08](Objects_LeaveRule.LeaveRule.md#f08)
- [F09](Objects_LeaveRule.LeaveRule.md#f09)
- [Id](Objects_LeaveRule.LeaveRule.md#id)
- [MaxAllowedAnnually](Objects_LeaveRule.LeaveRule.md#maxallowedannually)
- [Modified](Objects_LeaveRule.LeaveRule.md#modified)
- [Name](Objects_LeaveRule.LeaveRule.md#name)
- [PaidLeave](Objects_LeaveRule.LeaveRule.md#paidleave)
- [PayoutOnTermination](Objects_LeaveRule.LeaveRule.md#payoutontermination)
- [PayrollCategory](Objects_LeaveRule.LeaveRule.md#payrollcategory)
- [ResetSchedule](Objects_LeaveRule.LeaveRule.md#resetschedule)
- [ResetType](Objects_LeaveRule.LeaveRule.md#resettype)
- [ResetValue](Objects_LeaveRule.LeaveRule.md#resetvalue)
- [SvcLeavePolicyId](Objects_LeaveRule.LeaveRule.md#svcleavepolicyid)
- [Type](Objects_LeaveRule.LeaveRule.md#type)
- [UnitType](Objects_LeaveRule.LeaveRule.md#unittype)
- [Visible](Objects_LeaveRule.LeaveRule.md#visible)
- [\_DPMetaData](Objects_LeaveRule.LeaveRule.md#_dpmetadata)

## Properties

### AnnualRollOver

• **AnnualRollOver**: `boolean`

___

### Calc

• `Optional` **Calc**: `never`

___

### CalcType

• **CalcType**: `number`

___

### Created

• **Created**: `string` \| `Date`

The date this resource was created

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Created](Objects_BaseObject.BaseObject.md#created)

___

### Creator

• **Creator**: `number`

The deputy Id for the User that created this resource

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Creator](Objects_BaseObject.BaseObject.md#creator)

___

### Description

• **Description**: `string`

___

### EntitlementAfterMonth

• **EntitlementAfterMonth**: `number`

___

### ExportType

• **ExportType**: `number`

___

### F00

• `Optional` **F00**: `never`

___

### F01

• **F01**: `string`

___

### F02

• **F02**: `string`

___

### F03

• `Optional` **F03**: `never`

___

### F04

• `Optional` **F04**: `never`

___

### F05

• `Optional` **F05**: `never`

___

### F06

• `Optional` **F06**: `never`

___

### F07

• `Optional` **F07**: `never`

___

### F08

• `Optional` **F08**: `never`

___

### F09

• `Optional` **F09**: `never`

___

### Id

• **Id**: `number`

The deputy resource id

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Id](Objects_BaseObject.BaseObject.md#id)

___

### MaxAllowedAnnually

• **MaxAllowedAnnually**: `number`

___

### Modified

• **Modified**: `string` \| `Date`

The date this resource was last modified.

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Modified](Objects_BaseObject.BaseObject.md#modified)

___

### Name

• **Name**: `string`

___

### PaidLeave

• **PaidLeave**: `boolean`

___

### PayoutOnTermination

• **PayoutOnTermination**: `boolean`

___

### PayrollCategory

• **PayrollCategory**: `string`

___

### ResetSchedule

• `Optional` **ResetSchedule**: `never`

___

### ResetType

• `Optional` **ResetType**: `never`

___

### ResetValue

• `Optional` **ResetValue**: `never`

___

### SvcLeavePolicyId

• `Optional` **SvcLeavePolicyId**: `never`

___

### Type

• **Type**: `string`

___

### UnitType

• `Optional` **UnitType**: `never`

___

### Visible

• **Visible**: `boolean`

___

### \_DPMetaData

• **\_DPMetaData**: [`DPMetaData`](Objects_BaseObject.DPMetaData.md)
