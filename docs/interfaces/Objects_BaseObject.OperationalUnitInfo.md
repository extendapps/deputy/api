[Deputy API NetSuite module](../README.md) / [Objects/BaseObject](../modules/Objects_BaseObject.md) / OperationalUnitInfo

# Interface: OperationalUnitInfo

[Objects/BaseObject](../modules/Objects_BaseObject.md).OperationalUnitInfo

## Table of contents

### Properties

- [Company](Objects_BaseObject.OperationalUnitInfo.md#company)
- [CompanyName](Objects_BaseObject.OperationalUnitInfo.md#companyname)
- [Id](Objects_BaseObject.OperationalUnitInfo.md#id)
- [LabelWithCompany](Objects_BaseObject.OperationalUnitInfo.md#labelwithcompany)
- [OperationalUnitName](Objects_BaseObject.OperationalUnitInfo.md#operationalunitname)

## Properties

### Company

• **Company**: `number`

___

### CompanyName

• **CompanyName**: `string`

___

### Id

• **Id**: `number`

___

### LabelWithCompany

• **LabelWithCompany**: `string`

___

### OperationalUnitName

• **OperationalUnitName**: `string`
