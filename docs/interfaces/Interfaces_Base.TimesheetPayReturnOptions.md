[Deputy API NetSuite module](../README.md) / [Interfaces/Base](../modules/Interfaces_Base.md) / TimesheetPayReturnOptions

# Interface: TimesheetPayReturnOptions<T\>

[Interfaces/Base](../modules/Interfaces_Base.md).TimesheetPayReturnOptions

## Type parameters

| Name |
| :------ |
| `T` |

## Hierarchy

- `BaseOptions`

  ↳ **`TimesheetPayReturnOptions`**

## Table of contents

### Properties

- [endDate](Interfaces_Base.TimesheetPayReturnOptions.md#enddate)
- [excludeExported](Interfaces_Base.TimesheetPayReturnOptions.md#excludeexported)
- [operationalUnitIds](Interfaces_Base.TimesheetPayReturnOptions.md#operationalunitids)
- [startDate](Interfaces_Base.TimesheetPayReturnOptions.md#startdate)

### Methods

- [Failed](Interfaces_Base.TimesheetPayReturnOptions.md#failed)
- [OK](Interfaces_Base.TimesheetPayReturnOptions.md#ok)

## Properties

### endDate

• **endDate**: `string`

___

### excludeExported

• **excludeExported**: `boolean`

___

### operationalUnitIds

• `Optional` **operationalUnitIds**: `string`[]

___

### startDate

• **startDate**: `string`

## Methods

### Failed

▸ **Failed**(`clientResponse`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `clientResponse` | `ClientResponse` |

#### Returns

`void`

#### Inherited from

BaseOptions.Failed

___

### OK

▸ **OK**(`objects`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `objects` | `T`[] |

#### Returns

`void`
