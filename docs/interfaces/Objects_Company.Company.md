[Deputy API NetSuite module](../README.md) / [Objects/Company](../modules/Objects_Company.md) / Company

# Interface: Company

[Objects/Company](../modules/Objects_Company.md).Company

Company (otherwise known as Location)

## Hierarchy

- [`BaseObject`](Objects_BaseObject.BaseObject.md)

  ↳ **`Company`**

## Table of contents

### Properties

- [Active](Objects_Company.Company.md#active)
- [Address](Objects_Company.Company.md#address)
- [BusinessNumber](Objects_Company.Company.md#businessnumber)
- [Code](Objects_Company.Company.md#code)
- [CompanyName](Objects_Company.Company.md#companyname)
- [CompanyNumber](Objects_Company.Company.md#companynumber)
- [Contact](Objects_Company.Company.md#contact)
- [Created](Objects_Company.Company.md#created)
- [Creator](Objects_Company.Company.md#creator)
- [Id](Objects_Company.Company.md#id)
- [IsPayrollEntity](Objects_Company.Company.md#ispayrollentity)
- [IsWorkplace](Objects_Company.Company.md#isworkplace)
- [Modified](Objects_Company.Company.md#modified)
- [ParentCompany](Objects_Company.Company.md#parentcompany)
- [PayrollExportCode](Objects_Company.Company.md#payrollexportcode)
- [Portfolio](Objects_Company.Company.md#portfolio)
- [TradingName](Objects_Company.Company.md#tradingname)

## Properties

### Active

• **Active**: `boolean`

___

### Address

• **Address**: `number`

___

### BusinessNumber

• **BusinessNumber**: `string`

___

### Code

• **Code**: `string`

___

### CompanyName

• **CompanyName**: `string`

Name of the location/workplace

___

### CompanyNumber

• **CompanyNumber**: `string`

___

### Contact

• **Contact**: `number`

___

### Created

• **Created**: `string` \| `Date`

The date this resource was created

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Created](Objects_BaseObject.BaseObject.md#created)

___

### Creator

• **Creator**: `number`

The deputy Id for the User that created this resource

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Creator](Objects_BaseObject.BaseObject.md#creator)

___

### Id

• **Id**: `number`

The deputy resource id

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Id](Objects_BaseObject.BaseObject.md#id)

___

### IsPayrollEntity

• **IsPayrollEntity**: `boolean`

___

### IsWorkplace

• **IsWorkplace**: `boolean`

___

### Modified

• **Modified**: `string` \| `Date`

The date this resource was last modified.

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Modified](Objects_BaseObject.BaseObject.md#modified)

___

### ParentCompany

• `Optional` **ParentCompany**: `number`

___

### PayrollExportCode

• **PayrollExportCode**: `string`

___

### Portfolio

• `Optional` **Portfolio**: `number`

___

### TradingName

• **TradingName**: `string`
