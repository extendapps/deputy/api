[Deputy API NetSuite module](../README.md) / [Objects/BaseObject](../modules/Objects_BaseObject.md) / BaseObject

# Interface: BaseObject

[Objects/BaseObject](../modules/Objects_BaseObject.md).BaseObject

BaseObject is a common structure that all Deputy Resources share

## Hierarchy

- **`BaseObject`**

  ↳ [`Country`](Objects_Country.Country.md)

  ↳ [`State`](Objects_State.State.md)

  ↳ [`Employee`](Objects_Employee.Employee.md)

  ↳ [`Company`](Objects_Company.Company.md)

  ↳ [`PayRule`](Objects_PayRule.PayRule.md)

  ↳ [`TimesheetPayReturn`](Objects_TimesheetPayReturn.TimesheetPayReturn.md)

  ↳ [`TimesheetObject`](Objects_Timesheet.TimesheetObject.md)

  ↳ [`OperationalUnit`](Objects_OperationalUnit.OperationalUnit.md)

  ↳ [`LeaveRule`](Objects_LeaveRule.LeaveRule.md)

## Table of contents

### Properties

- [Created](Objects_BaseObject.BaseObject.md#created)
- [Creator](Objects_BaseObject.BaseObject.md#creator)
- [Id](Objects_BaseObject.BaseObject.md#id)
- [Modified](Objects_BaseObject.BaseObject.md#modified)

## Properties

### Created

• **Created**: `string` \| `Date`

The date this resource was created

___

### Creator

• **Creator**: `number`

The deputy Id for the User that created this resource

___

### Id

• **Id**: `number`

The deputy resource id

___

### Modified

• **Modified**: `string` \| `Date`

The date this resource was last modified.
