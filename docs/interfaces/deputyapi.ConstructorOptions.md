[Deputy API NetSuite module](../README.md) / [deputyapi](../modules/deputyapi.md) / ConstructorOptions

# Interface: ConstructorOptions

[deputyapi](../modules/deputyapi.md).ConstructorOptions

## Table of contents

### Properties

- [clientId](deputyapi.ConstructorOptions.md#clientid)
- [clientSecret](deputyapi.ConstructorOptions.md#clientsecret)
- [endPoint](deputyapi.ConstructorOptions.md#endpoint)
- [loggingEnabled](deputyapi.ConstructorOptions.md#loggingenabled)
- [newRefreshToken](deputyapi.ConstructorOptions.md#newrefreshtoken)
- [redirectURL](deputyapi.ConstructorOptions.md#redirecturl)
- [refreshToken](deputyapi.ConstructorOptions.md#refreshtoken)
- [version](deputyapi.ConstructorOptions.md#version)

## Properties

### clientId

• **clientId**: `string`

___

### clientSecret

• **clientSecret**: `string`

___

### endPoint

• `Optional` **endPoint**: `string`

___

### loggingEnabled

• `Optional` **loggingEnabled**: `boolean`

___

### newRefreshToken

• `Optional` **newRefreshToken**: [`NewRefreshTokenCallback`](../modules/deputyapi.md#newrefreshtokencallback)

___

### redirectURL

• `Optional` **redirectURL**: [`CustomURL`](../modules/deputyapi.md#customurl)

___

### refreshToken

• `Optional` **refreshToken**: `string`

___

### version

• `Optional` **version**: ``"v1"``
