[Deputy API NetSuite module](../README.md) / [Objects/Timesheet](../modules/Objects_Timesheet.md) / TimesheetObject

# Interface: TimesheetObject

[Objects/Timesheet](../modules/Objects_Timesheet.md).TimesheetObject

BaseObject is a common structure that all Deputy Resources share

## Hierarchy

- [`BaseObject`](Objects_BaseObject.BaseObject.md)

  ↳ **`TimesheetObject`**

## Table of contents

### Properties

- [AutoPayRuleApproved](Objects_Timesheet.TimesheetObject.md#autopayruleapproved)
- [AutoProcessed](Objects_Timesheet.TimesheetObject.md#autoprocessed)
- [AutoRounded](Objects_Timesheet.TimesheetObject.md#autorounded)
- [AutoTimeApproved](Objects_Timesheet.TimesheetObject.md#autotimeapproved)
- [Cost](Objects_Timesheet.TimesheetObject.md#cost)
- [Created](Objects_Timesheet.TimesheetObject.md#created)
- [Creator](Objects_Timesheet.TimesheetObject.md#creator)
- [CustomFieldData](Objects_Timesheet.TimesheetObject.md#customfielddata)
- [Date](Objects_Timesheet.TimesheetObject.md#date)
- [Discarded](Objects_Timesheet.TimesheetObject.md#discarded)
- [Disputed](Objects_Timesheet.TimesheetObject.md#disputed)
- [Employee](Objects_Timesheet.TimesheetObject.md#employee)
- [EmployeeAgreement](Objects_Timesheet.TimesheetObject.md#employeeagreement)
- [EmployeeComment](Objects_Timesheet.TimesheetObject.md#employeecomment)
- [EmployeeHistory](Objects_Timesheet.TimesheetObject.md#employeehistory)
- [EndTime](Objects_Timesheet.TimesheetObject.md#endtime)
- [EndTimeLocalized](Objects_Timesheet.TimesheetObject.md#endtimelocalized)
- [Exported](Objects_Timesheet.TimesheetObject.md#exported)
- [File](Objects_Timesheet.TimesheetObject.md#file)
- [Id](Objects_Timesheet.TimesheetObject.md#id)
- [InvoiceComment](Objects_Timesheet.TimesheetObject.md#invoicecomment)
- [Invoiced](Objects_Timesheet.TimesheetObject.md#invoiced)
- [IsInProgress](Objects_Timesheet.TimesheetObject.md#isinprogress)
- [IsLeave](Objects_Timesheet.TimesheetObject.md#isleave)
- [LeaveId](Objects_Timesheet.TimesheetObject.md#leaveid)
- [LeaveRule](Objects_Timesheet.TimesheetObject.md#leaverule)
- [Mealbreak](Objects_Timesheet.TimesheetObject.md#mealbreak)
- [MealbreakSlots](Objects_Timesheet.TimesheetObject.md#mealbreakslots)
- [Modified](Objects_Timesheet.TimesheetObject.md#modified)
- [OnCost](Objects_Timesheet.TimesheetObject.md#oncost)
- [OperationalUnit](Objects_Timesheet.TimesheetObject.md#operationalunit)
- [PayRuleApproved](Objects_Timesheet.TimesheetObject.md#payruleapproved)
- [PayStaged](Objects_Timesheet.TimesheetObject.md#paystaged)
- [PaycycleId](Objects_Timesheet.TimesheetObject.md#paycycleid)
- [RealTime](Objects_Timesheet.TimesheetObject.md#realtime)
- [Roster](Objects_Timesheet.TimesheetObject.md#roster)
- [Slots](Objects_Timesheet.TimesheetObject.md#slots)
- [StagingId](Objects_Timesheet.TimesheetObject.md#stagingid)
- [StartTime](Objects_Timesheet.TimesheetObject.md#starttime)
- [StartTimeLocalized](Objects_Timesheet.TimesheetObject.md#starttimelocalized)
- [Supervisor](Objects_Timesheet.TimesheetObject.md#supervisor)
- [SupervisorComment](Objects_Timesheet.TimesheetObject.md#supervisorcomment)
- [TimeApproved](Objects_Timesheet.TimesheetObject.md#timeapproved)
- [TimeApprover](Objects_Timesheet.TimesheetObject.md#timeapprover)
- [TotalTime](Objects_Timesheet.TimesheetObject.md#totaltime)
- [TotalTimeInv](Objects_Timesheet.TimesheetObject.md#totaltimeinv)
- [ValidationFlag](Objects_Timesheet.TimesheetObject.md#validationflag)
- [\_DPMetaData](Objects_Timesheet.TimesheetObject.md#_dpmetadata)

## Properties

### AutoPayRuleApproved

• **AutoPayRuleApproved**: `boolean`

___

### AutoProcessed

• **AutoProcessed**: `boolean`

___

### AutoRounded

• **AutoRounded**: `boolean`

___

### AutoTimeApproved

• **AutoTimeApproved**: `boolean`

___

### Cost

• **Cost**: `number`

___

### Created

• **Created**: `string` \| `Date`

The date this resource was created

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Created](Objects_BaseObject.BaseObject.md#created)

___

### Creator

• **Creator**: `number`

The deputy Id for the User that created this resource

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Creator](Objects_BaseObject.BaseObject.md#creator)

___

### CustomFieldData

• `Optional` **CustomFieldData**: `any`

___

### Date

• **Date**: `string` \| `Date`

___

### Discarded

• `Optional` **Discarded**: `any`

___

### Disputed

• **Disputed**: `boolean`

___

### Employee

• **Employee**: `number`

___

### EmployeeAgreement

• **EmployeeAgreement**: `number`

___

### EmployeeComment

• `Optional` **EmployeeComment**: `any`

___

### EmployeeHistory

• **EmployeeHistory**: `number`

___

### EndTime

• **EndTime**: `number`

___

### EndTimeLocalized

• **EndTimeLocalized**: `string` \| `Date`

___

### Exported

• `Optional` **Exported**: `any`

___

### File

• `Optional` **File**: `any`

___

### Id

• **Id**: `number`

The deputy resource id

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Id](Objects_BaseObject.BaseObject.md#id)

___

### InvoiceComment

• `Optional` **InvoiceComment**: `any`

___

### Invoiced

• **Invoiced**: `boolean`

___

### IsInProgress

• `Optional` **IsInProgress**: `any`

___

### IsLeave

• **IsLeave**: `boolean`

___

### LeaveId

• `Optional` **LeaveId**: `any`

___

### LeaveRule

• `Optional` **LeaveRule**: `number`

___

### Mealbreak

• **Mealbreak**: `string` \| `Date`

___

### MealbreakSlots

• **MealbreakSlots**: `string`

___

### Modified

• **Modified**: `string` \| `Date`

The date this resource was last modified.

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Modified](Objects_BaseObject.BaseObject.md#modified)

___

### OnCost

• **OnCost**: `number`

___

### OperationalUnit

• **OperationalUnit**: `number`

___

### PayRuleApproved

• **PayRuleApproved**: `boolean`

___

### PayStaged

• **PayStaged**: `boolean`

___

### PaycycleId

• **PaycycleId**: `number`

___

### RealTime

• **RealTime**: `boolean`

___

### Roster

• `Optional` **Roster**: `any`

___

### Slots

• **Slots**: [`Slot`](Objects_Timesheet.Slot.md)[]

___

### StagingId

• `Optional` **StagingId**: `any`

___

### StartTime

• **StartTime**: `number`

___

### StartTimeLocalized

• **StartTimeLocalized**: `string` \| `Date`

___

### Supervisor

• `Optional` **Supervisor**: `any`

___

### SupervisorComment

• `Optional` **SupervisorComment**: `any`

___

### TimeApproved

• **TimeApproved**: `boolean`

___

### TimeApprover

• **TimeApprover**: `number`

___

### TotalTime

• **TotalTime**: `number`

___

### TotalTimeInv

• **TotalTimeInv**: `number`

___

### ValidationFlag

• **ValidationFlag**: `number`

___

### \_DPMetaData

• **\_DPMetaData**: [`DPMetaData`](Objects_BaseObject.DPMetaData.md)
