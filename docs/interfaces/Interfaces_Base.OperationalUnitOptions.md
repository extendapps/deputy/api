[Deputy API NetSuite module](../README.md) / [Interfaces/Base](../modules/Interfaces_Base.md) / OperationalUnitOptions

# Interface: OperationalUnitOptions<T\>

[Interfaces/Base](../modules/Interfaces_Base.md).OperationalUnitOptions

## Type parameters

| Name |
| :------ |
| `T` |

## Hierarchy

- `BaseOptions`

  ↳ **`OperationalUnitOptions`**

## Table of contents

### Properties

- [companyIds](Interfaces_Base.OperationalUnitOptions.md#companyids)

### Methods

- [Failed](Interfaces_Base.OperationalUnitOptions.md#failed)
- [OK](Interfaces_Base.OperationalUnitOptions.md#ok)

## Properties

### companyIds

• `Optional` **companyIds**: `string`[]

## Methods

### Failed

▸ **Failed**(`clientResponse`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `clientResponse` | `ClientResponse` |

#### Returns

`void`

#### Inherited from

BaseOptions.Failed

___

### OK

▸ **OK**(`objects`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `objects` | `T`[] |

#### Returns

`void`
