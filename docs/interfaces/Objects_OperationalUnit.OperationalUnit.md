[Deputy API NetSuite module](../README.md) / [Objects/OperationalUnit](../modules/Objects_OperationalUnit.md) / OperationalUnit

# Interface: OperationalUnit

[Objects/OperationalUnit](../modules/Objects_OperationalUnit.md).OperationalUnit

BaseObject is a common structure that all Deputy Resources share

## Hierarchy

- [`BaseObject`](Objects_BaseObject.BaseObject.md)

  ↳ **`OperationalUnit`**

## Table of contents

### Properties

- [Active](Objects_OperationalUnit.OperationalUnit.md#active)
- [Address](Objects_OperationalUnit.OperationalUnit.md#address)
- [AddressObject](Objects_OperationalUnit.OperationalUnit.md#addressobject)
- [Colour](Objects_OperationalUnit.OperationalUnit.md#colour)
- [Company](Objects_OperationalUnit.OperationalUnit.md#company)
- [CompanyCode](Objects_OperationalUnit.OperationalUnit.md#companycode)
- [CompanyName](Objects_OperationalUnit.OperationalUnit.md#companyname)
- [Contact](Objects_OperationalUnit.OperationalUnit.md#contact)
- [Created](Objects_OperationalUnit.OperationalUnit.md#created)
- [Creator](Objects_OperationalUnit.OperationalUnit.md#creator)
- [DailyRosterBudget](Objects_OperationalUnit.OperationalUnit.md#dailyrosterbudget)
- [Id](Objects_OperationalUnit.OperationalUnit.md#id)
- [Modified](Objects_OperationalUnit.OperationalUnit.md#modified)
- [OperationalUnitName](Objects_OperationalUnit.OperationalUnit.md#operationalunitname)
- [OperationalUnitType](Objects_OperationalUnit.OperationalUnit.md#operationalunittype)
- [ParentOperationalUnit](Objects_OperationalUnit.OperationalUnit.md#parentoperationalunit)
- [PayrollExportName](Objects_OperationalUnit.OperationalUnit.md#payrollexportname)
- [RosterActiveHoursSchedule](Objects_OperationalUnit.OperationalUnit.md#rosteractivehoursschedule)
- [RosterSortOrder](Objects_OperationalUnit.OperationalUnit.md#rostersortorder)
- [ShowOnRoster](Objects_OperationalUnit.OperationalUnit.md#showonroster)
- [WorkType](Objects_OperationalUnit.OperationalUnit.md#worktype)
- [\_DPMetaData](Objects_OperationalUnit.OperationalUnit.md#_dpmetadata)

## Properties

### Active

• **Active**: `boolean`

___

### Address

• **Address**: `number`

___

### AddressObject

• **AddressObject**: `any`[]

___

### Colour

• **Colour**: `string`

___

### Company

• **Company**: `number`

___

### CompanyCode

• **CompanyCode**: `string`

___

### CompanyName

• **CompanyName**: `string`

___

### Contact

• `Optional` **Contact**: `any`

___

### Created

• **Created**: `string` \| `Date`

The date this resource was created

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Created](Objects_BaseObject.BaseObject.md#created)

___

### Creator

• **Creator**: `number`

The deputy Id for the User that created this resource

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Creator](Objects_BaseObject.BaseObject.md#creator)

___

### DailyRosterBudget

• `Optional` **DailyRosterBudget**: `any`

___

### Id

• **Id**: `number`

The deputy resource id

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Id](Objects_BaseObject.BaseObject.md#id)

___

### Modified

• **Modified**: `string` \| `Date`

The date this resource was last modified.

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Modified](Objects_BaseObject.BaseObject.md#modified)

___

### OperationalUnitName

• **OperationalUnitName**: `string`

___

### OperationalUnitType

• `Optional` **OperationalUnitType**: `any`

___

### ParentOperationalUnit

• **ParentOperationalUnit**: `number`

___

### PayrollExportName

• **PayrollExportName**: `string`

___

### RosterActiveHoursSchedule

• `Optional` **RosterActiveHoursSchedule**: `any`

___

### RosterSortOrder

• **RosterSortOrder**: `number`

___

### ShowOnRoster

• **ShowOnRoster**: `boolean`

___

### WorkType

• `Optional` **WorkType**: `any`

___

### \_DPMetaData

• **\_DPMetaData**: [`DPMetaData`](Objects_BaseObject.DPMetaData.md)
