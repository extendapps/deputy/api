[Deputy API NetSuite module](../README.md) / [Objects/Timesheet](../modules/Objects_Timesheet.md) / Slot

# Interface: Slot

[Objects/Timesheet](../modules/Objects_Timesheet.md).Slot

## Table of contents

### Properties

- [blnEmptySlot](Objects_Timesheet.Slot.md#blnemptyslot)
- [intEnd](Objects_Timesheet.Slot.md#intend)
- [intStart](Objects_Timesheet.Slot.md#intstart)
- [intUnixEnd](Objects_Timesheet.Slot.md#intunixend)
- [intUnixStart](Objects_Timesheet.Slot.md#intunixstart)
- [mixedActivity](Objects_Timesheet.Slot.md#mixedactivity)
- [strState](Objects_Timesheet.Slot.md#strstate)
- [strType](Objects_Timesheet.Slot.md#strtype)
- [strTypeName](Objects_Timesheet.Slot.md#strtypename)

## Properties

### blnEmptySlot

• **blnEmptySlot**: `boolean`

___

### intEnd

• **intEnd**: `number`

___

### intStart

• **intStart**: `number`

___

### intUnixEnd

• **intUnixEnd**: `number`

___

### intUnixStart

• **intUnixStart**: `number`

___

### mixedActivity

• **mixedActivity**: [`MixedActivity`](Objects_Timesheet.MixedActivity.md)

___

### strState

• **strState**: `string`

___

### strType

• **strType**: `string`

___

### strTypeName

• **strTypeName**: `string`
