[Deputy API NetSuite module](../README.md) / [Objects/Employee](../modules/Objects_Employee.md) / UpsertEmployee

# Interface: UpsertEmployee

[Objects/Employee](../modules/Objects_Employee.md).UpsertEmployee

UpsertEmployee [This is used to upsert Employees to Deputy]

## Table of contents

### Properties

- [blnSendInvite](Objects_Employee.UpsertEmployee.md#blnsendinvite)
- [intCompanyId](Objects_Employee.UpsertEmployee.md#intcompanyid)
- [intGender](Objects_Employee.UpsertEmployee.md#intgender)
- [intRoleId](Objects_Employee.UpsertEmployee.md#introleid)
- [strCity](Objects_Employee.UpsertEmployee.md#strcity)
- [strCountryCode](Objects_Employee.UpsertEmployee.md#strcountrycode)
- [strDob](Objects_Employee.UpsertEmployee.md#strdob)
- [strEmail](Objects_Employee.UpsertEmployee.md#stremail)
- [strEmergencyAddressContactName](Objects_Employee.UpsertEmployee.md#stremergencyaddresscontactname)
- [strEmergencyAddressPhone](Objects_Employee.UpsertEmployee.md#stremergencyaddressphone)
- [strFirstName](Objects_Employee.UpsertEmployee.md#strfirstname)
- [strLastName](Objects_Employee.UpsertEmployee.md#strlastname)
- [strMobilePhone](Objects_Employee.UpsertEmployee.md#strmobilephone)
- [strPostCode](Objects_Employee.UpsertEmployee.md#strpostcode)
- [strStartDate](Objects_Employee.UpsertEmployee.md#strstartdate)
- [strState](Objects_Employee.UpsertEmployee.md#strstate)
- [strStreet](Objects_Employee.UpsertEmployee.md#strstreet)

## Properties

### blnSendInvite

• `Optional` **blnSendInvite**: [`SendInvite`](../modules/Objects_Employee.md#sendinvite)

___

### intCompanyId

• **intCompanyId**: `number`

ID of a location in Deputy

___

### intGender

• **intGender**: [`Gender`](../modules/Objects_Employee.md#gender)

___

### intRoleId

• `Optional` **intRoleId**: `number`

Role ID [maximum characters: 20]

___

### strCity

• `Optional` **strCity**: `string`

___

### strCountryCode

• `Optional` **strCountryCode**: `string`

___

### strDob

• `Optional` **strDob**: `string`

___

### strEmail

• `Optional` **strEmail**: `string`

___

### strEmergencyAddressContactName

• `Optional` **strEmergencyAddressContactName**: `string`

___

### strEmergencyAddressPhone

• `Optional` **strEmergencyAddressPhone**: `string`

___

### strFirstName

• **strFirstName**: `string`

First name [maximum characters: 64]

___

### strLastName

• **strLastName**: `string`

Last name [maximum characters: 64]

___

### strMobilePhone

• `Optional` **strMobilePhone**: `string`

___

### strPostCode

• `Optional` **strPostCode**: `string`

___

### strStartDate

• `Optional` **strStartDate**: `string`

A string of the employee start date in the format YYYY-MM-DD

___

### strState

• `Optional` **strState**: `string`

___

### strStreet

• `Optional` **strStreet**: `string`
