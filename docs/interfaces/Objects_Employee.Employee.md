[Deputy API NetSuite module](../README.md) / [Objects/Employee](../modules/Objects_Employee.md) / Employee

# Interface: Employee

[Objects/Employee](../modules/Objects_Employee.md).Employee

BaseObject is a common structure that all Deputy Resources share

## Hierarchy

- [`BaseObject`](Objects_BaseObject.BaseObject.md)

  ↳ **`Employee`**

## Table of contents

### Properties

- [Active](Objects_Employee.Employee.md#active)
- [AllowAppraisal](Objects_Employee.Employee.md#allowappraisal)
- [Company](Objects_Employee.Employee.md#company)
- [Contact](Objects_Employee.Employee.md#contact)
- [Created](Objects_Employee.Employee.md#created)
- [Creator](Objects_Employee.Employee.md#creator)
- [CustomFieldData](Objects_Employee.Employee.md#customfielddata)
- [DateOfBirth](Objects_Employee.Employee.md#dateofbirth)
- [DisplayName](Objects_Employee.Employee.md#displayname)
- [Email](Objects_Employee.Employee.md#email)
- [EmergencyAddress](Objects_Employee.Employee.md#emergencyaddress)
- [FirstName](Objects_Employee.Employee.md#firstname)
- [Gender](Objects_Employee.Employee.md#gender)
- [HigherDuty](Objects_Employee.Employee.md#higherduty)
- [HistoryId](Objects_Employee.Employee.md#historyid)
- [Id](Objects_Employee.Employee.md#id)
- [JobAppId](Objects_Employee.Employee.md#jobappid)
- [LastName](Objects_Employee.Employee.md#lastname)
- [MainAddress](Objects_Employee.Employee.md#mainaddress)
- [Modified](Objects_Employee.Employee.md#modified)
- [OtherName](Objects_Employee.Employee.md#othername)
- [Photo](Objects_Employee.Employee.md#photo)
- [Position](Objects_Employee.Employee.md#position)
- [PostalAddress](Objects_Employee.Employee.md#postaladdress)
- [Role](Objects_Employee.Employee.md#role)
- [Salutation](Objects_Employee.Employee.md#salutation)
- [StartDate](Objects_Employee.Employee.md#startdate)
- [StressProfile](Objects_Employee.Employee.md#stressprofile)
- [TerminationDate](Objects_Employee.Employee.md#terminationdate)
- [UserId](Objects_Employee.Employee.md#userid)

## Properties

### Active

• **Active**: `boolean`

___

### AllowAppraisal

• **AllowAppraisal**: `boolean`

___

### Company

• **Company**: `number`

___

### Contact

• **Contact**: `number`

___

### Created

• **Created**: `string` \| `Date`

The date this resource was created

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Created](Objects_BaseObject.BaseObject.md#created)

___

### Creator

• **Creator**: `number`

The deputy Id for the User that created this resource

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Creator](Objects_BaseObject.BaseObject.md#creator)

___

### CustomFieldData

• **CustomFieldData**: `number`

___

### DateOfBirth

• `Optional` **DateOfBirth**: `string`

___

### DisplayName

• **DisplayName**: `string`

___

### Email

• **Email**: `string`

___

### EmergencyAddress

• **EmergencyAddress**: `number`

___

### FirstName

• **FirstName**: `string`

___

### Gender

• **Gender**: [`Gender`](../modules/Objects_Employee.md#gender)

___

### HigherDuty

• `Optional` **HigherDuty**: `string`

___

### HistoryId

• **HistoryId**: `number`

___

### Id

• **Id**: `number`

The deputy resource id

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Id](Objects_BaseObject.BaseObject.md#id)

___

### JobAppId

• `Optional` **JobAppId**: `string`

___

### LastName

• **LastName**: `string`

___

### MainAddress

• **MainAddress**: `number`

___

### Modified

• **Modified**: `string` \| `Date`

The date this resource was last modified.

#### Inherited from

[BaseObject](Objects_BaseObject.BaseObject.md).[Modified](Objects_BaseObject.BaseObject.md#modified)

___

### OtherName

• `Optional` **OtherName**: `string`

___

### Photo

• **Photo**: `number`

___

### Position

• `Optional` **Position**: `string`

___

### PostalAddress

• `Optional` **PostalAddress**: `string`

___

### Role

• **Role**: `number`

___

### Salutation

• `Optional` **Salutation**: `string`

___

### StartDate

• **StartDate**: `Date`

___

### StressProfile

• **StressProfile**: `number`

___

### TerminationDate

• `Optional` **TerminationDate**: `string`

___

### UserId

• **UserId**: `number`
