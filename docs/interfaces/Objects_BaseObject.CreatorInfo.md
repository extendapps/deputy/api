[Deputy API NetSuite module](../README.md) / [Objects/BaseObject](../modules/Objects_BaseObject.md) / CreatorInfo

# Interface: CreatorInfo

[Objects/BaseObject](../modules/Objects_BaseObject.md).CreatorInfo

## Table of contents

### Properties

- [CustomPronouns](Objects_BaseObject.CreatorInfo.md#custompronouns)
- [DisplayName](Objects_BaseObject.CreatorInfo.md#displayname)
- [Employee](Objects_BaseObject.CreatorInfo.md#employee)
- [EmployeeProfile](Objects_BaseObject.CreatorInfo.md#employeeprofile)
- [Id](Objects_BaseObject.CreatorInfo.md#id)
- [Photo](Objects_BaseObject.CreatorInfo.md#photo)
- [Pronouns](Objects_BaseObject.CreatorInfo.md#pronouns)

## Properties

### CustomPronouns

• **CustomPronouns**: `string`

___

### DisplayName

• **DisplayName**: `string`

___

### Employee

• **Employee**: `number`

___

### EmployeeProfile

• **EmployeeProfile**: `number`

___

### Id

• **Id**: `number`

___

### Photo

• **Photo**: `string`

___

### Pronouns

• **Pronouns**: `number`
