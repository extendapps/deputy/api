[Deputy API NetSuite module](../README.md) / Objects/BaseObject

# Module: Objects/BaseObject

## Table of contents

### Interfaces

- [BaseObject](../interfaces/Objects_BaseObject.BaseObject.md)
- [CreatorInfo](../interfaces/Objects_BaseObject.CreatorInfo.md)
- [DPMetaData](../interfaces/Objects_BaseObject.DPMetaData.md)
- [EmployeeInfo](../interfaces/Objects_BaseObject.EmployeeInfo.md)
- [OperationalUnitInfo](../interfaces/Objects_BaseObject.OperationalUnitInfo.md)
