[Deputy API NetSuite module](../README.md) / Objects/OperationalUnit

# Module: Objects/OperationalUnit

## Table of contents

### Interfaces

- [OperationalUnit](../interfaces/Objects_OperationalUnit.OperationalUnit.md)
