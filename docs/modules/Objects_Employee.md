[Deputy API NetSuite module](../README.md) / Objects/Employee

# Module: Objects/Employee

## Table of contents

### Interfaces

- [Employee](../interfaces/Objects_Employee.Employee.md)
- [UpsertEmployee](../interfaces/Objects_Employee.UpsertEmployee.md)

### Type Aliases

- [Gender](Objects_Employee.md#gender)
- [SendInvite](Objects_Employee.md#sendinvite)

## Type Aliases

### Gender

Ƭ `Readonly` **Gender**: ``0`` \| ``1`` \| ``2`` \| ``3``

Gender values

___

### SendInvite

Ƭ `Readonly` **SendInvite**: ``0`` \| ``1``

SendInvite values
