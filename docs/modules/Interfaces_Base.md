[Deputy API NetSuite module](../README.md) / Interfaces/Base

# Module: Interfaces/Base

## Table of contents

### Method Interfaces

- [EmployeeOptions](../interfaces/Interfaces_Base.EmployeeOptions.md)
- [GetObjectOptions](../interfaces/Interfaces_Base.GetObjectOptions.md)
- [GetObjectsOptions](../interfaces/Interfaces_Base.GetObjectsOptions.md)
- [OperationalUnitOptions](../interfaces/Interfaces_Base.OperationalUnitOptions.md)
- [TimesheetExportStatusOptions](../interfaces/Interfaces_Base.TimesheetExportStatusOptions.md)
- [TimesheetPayReturnOptions](../interfaces/Interfaces_Base.TimesheetPayReturnOptions.md)
- [UpsertObjectOptions](../interfaces/Interfaces_Base.UpsertObjectOptions.md)
- [UpsertObjectsOptions](../interfaces/Interfaces_Base.UpsertObjectsOptions.md)
