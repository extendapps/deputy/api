[Deputy API NetSuite module](../README.md) / deputyapi

# Module: deputyapi

## Table of contents

### Classes

- [DeputyAPI](../classes/deputyapi.DeputyAPI.md)

### Interfaces

- [ConstructorOptions](../interfaces/deputyapi.ConstructorOptions.md)

### Type Aliases

- [CustomURL](deputyapi.md#customurl)
- [NewRefreshTokenCallback](deputyapi.md#newrefreshtokencallback)

## Type Aliases

### CustomURL

Ƭ **CustomURL**: () => `string`

#### Type declaration

▸ (): `string`

##### Returns

`string`

___

### NewRefreshTokenCallback

Ƭ **NewRefreshTokenCallback**: (`endPoint`: `string`, `refreshToken`: `string`) => `void`

#### Type declaration

▸ (`endPoint`, `refreshToken`): `void`

##### Parameters

| Name | Type |
| :------ | :------ |
| `endPoint` | `string` |
| `refreshToken` | `string` |

##### Returns

`void`
