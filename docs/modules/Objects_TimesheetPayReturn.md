[Deputy API NetSuite module](../README.md) / Objects/TimesheetPayReturn

# Module: Objects/TimesheetPayReturn

## Table of contents

### Interfaces

- [TimesheetPayReturn](../interfaces/Objects_TimesheetPayReturn.TimesheetPayReturn.md)
