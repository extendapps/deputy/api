[Deputy API NetSuite module](../README.md) / Objects/Timesheet

# Module: Objects/Timesheet

## Table of contents

### Interfaces

- [MixedActivity](../interfaces/Objects_Timesheet.MixedActivity.md)
- [Slot](../interfaces/Objects_Timesheet.Slot.md)
- [TimesheetObject](../interfaces/Objects_Timesheet.TimesheetObject.md)
