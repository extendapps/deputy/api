/**
 * @copyright 2021 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {BaseObject} from './BaseObject';

export interface Country extends BaseObject {
    Code: string;
    CodeA3: string;
    Region: string;
    Active: boolean;
    SortOrder?: number;
    Country: string;
    ZipValidatePreg: string;
    PhoneDisplayPreg: string;
}
