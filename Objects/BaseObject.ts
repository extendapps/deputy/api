/**
 * @copyright 2021 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

/**
 * BaseObject is a common structure that all Deputy Resources share
 * @property {number} Id - The deputy resource id
 * @property {number} Creator - The deputy Id for the User that created this resource
 * @property {Date} Created - The date this resource was created
 * @property {Date} Modified - The date this resource was last modified.
 */
export interface BaseObject {
    Id: number;
    Creator: number;
    Created: Date | string;
    Modified: Date | string;
}

export interface DPMetaData {
    System: string;
    CreatorInfo: CreatorInfo;
    OperationalUnitInfo?: OperationalUnitInfo;
    EmployeeInfo?: EmployeeInfo;
    CustomFieldData?: any;
}

export interface OperationalUnitInfo {
    Id: number;
    OperationalUnitName: string;
    Company: number;
    CompanyName: string;
    LabelWithCompany: string;
}

export interface EmployeeInfo {
    Id: number;
    DisplayName: string;
    EmployeeProfile: number;
    Employee: number;
    Photo: string;
    Pronouns: number;
    CustomPronouns: string;
}

export interface CreatorInfo {
    Id: number;
    DisplayName: string;
    EmployeeProfile: number;
    Employee: number;
    Photo: string;
    Pronouns: number;
    CustomPronouns: string;
}
