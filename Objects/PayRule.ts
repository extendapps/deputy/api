/**
 * @copyright 2021 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {BaseObject, DPMetaData} from './BaseObject';

export interface PayRule extends BaseObject {
    PayTitle: string;
    RemunerationType: 1 | 3 | 2; // Integer *Required [ Hourly = 1; Unit = 3; Salary = 2; ]
    RemunerationBy: 1 | 2 | 3; // Integer *Required [ Base = 1; Shift Loading = 2; Period Loading = 3; ]
    IsExported: boolean;
    AnnualSalary?: number;
    HourlyRate?: number;
    IsMultiplier: boolean;
    MultiplierValue?: number;
    MultiplierBaseRate?: number;
    MinimumType?: number;
    MaximumType?: number;
    MinimumValue?: number;
    MaximumValue?: number;
    MinimumShiftLength?: number;
    MaximumShiftLength?: number;
    AdvancedCalculation?: number;
    UnitValue?: number;
    Schedule?: number;
    RecommendWith?: number;
    DexmlScript?: string;
    DexmlScriptParam?: string;
    PeriodType?: string;
    PayPortionRule?: number;
    PayrollCategory?: string;
    Comment?: string;
    _DPMetaData: DPMetaData;
}
