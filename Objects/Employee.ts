/**
 * @copyright 2021 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {BaseObject} from './BaseObject';

export interface Employee extends BaseObject {
    Company: number;
    FirstName: string;
    LastName: string;
    DisplayName: string;
    OtherName?: string;
    Salutation?: string;
    MainAddress: number;
    PostalAddress?: string;
    Contact: number;
    EmergencyAddress: number;
    DateOfBirth?: string;
    Gender: Gender;
    Photo: number;
    UserId: number;
    JobAppId?: string;
    Active: boolean;
    StartDate: Date;
    TerminationDate?: string;
    StressProfile: number;
    Position?: string;
    HigherDuty?: string;
    Role: number;
    AllowAppraisal: boolean;
    HistoryId: number;
    CustomFieldData: number;
    Email: string;
}

/**
 * UpsertEmployee [This is used to upsert Employees to Deputy]
 * @property {string} strFirstName - First name [maximum characters: 64]
 * @property {string} strLastName - Last name [maximum characters: 64]
 * @property {number} intCompanyId - ID of a location in Deputy
 * @property {number} [intRoleId] - Role ID [maximum characters: 20]
 * @property {Gender} intGender=0 - Gender
 * @property {string} strStartDate - A string of the employee start date in the format YYYY-MM-DD
 */
export interface UpsertEmployee {
    strFirstName: string; // maximum characters: 64
    strLastName: string; // maximum characters: 64
    intCompanyId: number; // ID of a location in Deputy
    intRoleId?: number;
    intGender: Gender;
    strDob?: string; // A string of the employee date of birth in the format YYYY-MM-DD
    strStartDate?: string; // YYYY-MM-DD
    strMobilePhone?: string; //A string of the employees mobile phone number
    strStreet?: string; // maximum characters: 64
    strCountryCode?: string; // In practice, we found using the 2 character country-code is correct.  (ie.e CA = Canada)
    strState?: string; // maximum characters: 64
    strCity?: string; // maximum characters: 64
    strPostCode?: string; // maximum characters: 20
    strEmergencyAddressContactName?: string; // maximum characters: 255
    strEmergencyAddressPhone?: string; // maximum characters: 32
    strEmail?: string; // maximum characters: 255
    blnSendInvite?: SendInvite;
}

/**
 * Gender values
 * @readonly
 * @enum {number=0, 1, 2, 3}
 */
export type Gender = 0 | 1 | 2 | 3; // 0 = Prefer not to say, 1 = Male, 2 = Female, 3 = Non-binary [default = 1]

/**
 * SendInvite values
 * @readonly
 * @enum {number}
 */
export type SendInvite = 0 | 1; // 0 = No, 1 = Yes [default: 1]
