/**
 * @copyright 2021 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {BaseObject} from './BaseObject';

export interface State extends BaseObject {
    Country: number;
    Code: string;
    Active: boolean;
    SortOrder?: number;
    State: string;
}
