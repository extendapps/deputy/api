/**
 * @copyright 2021 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {BaseObject, DPMetaData} from './BaseObject';

export interface LeaveRule extends BaseObject {
    SvcLeavePolicyId?: never;
    Name: string;
    Type: string;
    Description: string;
    MaxAllowedAnnually: number;
    PaidLeave: boolean;
    AnnualRollOver: boolean;
    Visible: boolean;
    UnitType?: never;
    ResetType?: never;
    ResetSchedule?: never;
    ResetValue?: never;
    PayoutOnTermination: boolean;
    EntitlementAfterMonth: number;
    ExportType: number;
    PayrollCategory: string;
    CalcType: number;
    Calc?: never;
    F00?: never;
    F01: string;
    F02: string;
    F03?: never;
    F04?: never;
    F05?: never;
    F06?: never;
    F07?: never;
    F08?: never;
    F09?: never;
    _DPMetaData: DPMetaData;
}
