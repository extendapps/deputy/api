/**
 * @copyright 2021 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {BaseObject, DPMetaData} from './BaseObject';

export interface TimesheetObject extends BaseObject {
    Employee: number;
    EmployeeHistory: number;
    EmployeeAgreement: number;
    Date: Date | string;
    StartTime: number;
    EndTime: number;
    Mealbreak: Date | string;
    MealbreakSlots: string;
    Slots: Slot[];
    TotalTime: number;
    TotalTimeInv: number;
    Cost: number;
    Roster?: any;
    EmployeeComment?: any;
    SupervisorComment?: any;
    Supervisor?: any;
    Disputed: boolean;
    TimeApproved: boolean;
    TimeApprover: number;
    Discarded?: any;
    ValidationFlag: number;
    OperationalUnit: number;
    IsInProgress?: any;
    IsLeave: boolean;
    LeaveId?: any;
    LeaveRule?: number;
    Invoiced: boolean;
    InvoiceComment?: any;
    PayRuleApproved: boolean;
    Exported?: any;
    StagingId?: any;
    PayStaged: boolean;
    PaycycleId: number;
    File?: any;
    CustomFieldData?: any;
    RealTime: boolean;
    AutoProcessed: boolean;
    AutoRounded: boolean;
    AutoTimeApproved: boolean;
    AutoPayRuleApproved: boolean;
    OnCost: number;
    StartTimeLocalized: Date | string;
    EndTimeLocalized: Date | string;
    _DPMetaData: DPMetaData;
}

export interface Slot {
    blnEmptySlot: boolean;
    strType: string;
    intStart: number;
    intEnd: number;
    intUnixStart: number;
    intUnixEnd: number;
    mixedActivity: MixedActivity;
    strTypeName: string;
    strState: string;
}

export interface MixedActivity {
    intState: number;
    blnCanStartEarly: number;
    blnCanEndEarly: number;
    blnIsMandatory: number;
    strBreakType: string;
}
