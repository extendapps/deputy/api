/**
 * @copyright 2021 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {BaseObject, DPMetaData} from './BaseObject';

export interface OperationalUnit extends BaseObject {
    Company: number;
    WorkType?: any;
    ParentOperationalUnit: number;
    OperationalUnitName: string;
    Active: boolean;
    PayrollExportName: string;
    Address: number;
    Contact?: any;
    RosterSortOrder: number;
    ShowOnRoster: boolean;
    Colour: string;
    RosterActiveHoursSchedule?: any;
    DailyRosterBudget?: any;
    OperationalUnitType?: any;
    CompanyCode: string;
    CompanyName: string;
    AddressObject: any[];
    _DPMetaData: DPMetaData;
}
