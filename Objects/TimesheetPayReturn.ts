/**
 * @copyright 2021 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {BaseObject, DPMetaData} from './BaseObject';
import {PayRule} from './PayRule';
import {TimesheetObject} from './Timesheet';

export interface TimesheetPayReturn extends BaseObject {
    Timesheet: number;
    PayRule: number;
    Overridden: boolean;
    Value: number;
    Cost: number;
    OverrideComment: string;
    _DPMetaData: DPMetaData;
    PayRuleObject: PayRule;
    TimesheetObject: TimesheetObject;
}
