/**
 * @copyright 2021 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {BaseObject} from './BaseObject';

/**
 * Company (otherwise known as Location)
 * @property {number} [Portfolio] -
 * @property {string} Code -
 * @property {boolean} Active -
 * @property {number} [ParentCompany=0] -
 * @property {string} CompanyName - Name of the location/workplace
 * @property {string} [TradingName]
 */
export interface Company extends BaseObject {
    Portfolio?: number;
    Code: string;
    Active: boolean;
    ParentCompany?: number;
    CompanyName: string;
    TradingName: string;
    BusinessNumber: string;
    CompanyNumber: string;
    IsWorkplace: boolean;
    IsPayrollEntity: boolean;
    PayrollExportCode: string;
    Address: number;
    Contact: number;
}
