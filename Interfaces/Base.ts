/**
 * @copyright 2021 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import * as https from 'N/https';

/**
 * @category Method
 * @hidden
 * */
export interface BaseOptions {
    Failed(clientResponse: https.ClientResponse): void;
}

/** @category Method */
export interface GetObjectsOptions<T> extends BaseOptions {
    OK(objects: T[]): void;
}

/** @category Method */
export interface GetObjectOptions<T> extends BaseOptions {
    objectId: number;

    OK(object: T): void;
}

/** @category Method */
export interface UpsertObjectOptions<T, Y> extends BaseOptions {
    objectId?: number;
    object: T;

    OK(object: Y): void;
}

/** @category Method */
export interface UpsertObjectsOptions<T, Y> extends BaseOptions {
    objects: T[];

    OK(objects: Y[]): void;
}

/** @category Method */
export interface EmployeeOptions<T> extends BaseOptions {
    isActive: boolean;

    OK(objects: T[]): void;
}

/** @category Method */
export interface TimesheetPayReturnOptions<T> extends BaseOptions {
    startDate: string; // 2022-01-01
    endDate: string; // 2022-01-01
    excludeExported: boolean;
    operationalUnitIds?: string[];

    OK(objects: T[]): void;
}

/** @category Method */
export interface OperationalUnitOptions<T> extends BaseOptions {
    companyIds?: string[];

    OK(objects: T[]): void;
}

/** @category Method */
export interface TimesheetExportStatusOptions<T> extends BaseOptions {
    timeSheetId: number;
    status: boolean;

    OK(object: T): void;
}
